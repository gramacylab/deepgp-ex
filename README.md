
# Deep Gaussian Process Examples

Maintainer: [Annie Booth](https://www.anniesbooth.com) (previously named Annie Sauer, <annie_booth@ncsu.edu>) with Steven Barnett (<sdbarnett@vt.edu>)

This directory primarily houses our work on Bayesian deep Gaussian processes. Throughout, we utilize the [deepgp](https://CRAN.R-project.org/package=deepgp) package on CRAN.  

This work has been published in the following manuscripts:

* Sauer, A. *Deep Gaussian process surrogates for computer experiments* (2023). Ph.D. Thesis, Department of Statistics, Virginia Polytechnic Institute and State University; http://hdl.handle.net/10919/114845
* Sauer, A., Gramacy, R. B., & Higdon, D. (2023).  Active learning for deep Gaussian process surrogates.  *Technometrics,* 65(1), 4-18. arXiv:2012.08015
* Sauer, A., Cooper, A., & Gramacy, R. B. (2023).  Vecchia-approximated deep Gaussian processes for computer experiments.  *Journal of Computational and Graphical Statistics, 32*(3), 824--837.  arXiv:2204.02904
* Sauer, A., Cooper, A., & Gramacy, R. B. (2023).  Non-stationary Gaussian process surrogates. *To appear in the Handbook of Uncertainty Quantification.* arXiv:2305.19242
* Booth, A. S., Renganathan S. A., & Gramacy, R. B. (2024).  Contour location for reliability in airfoil simulation experiments using deep Gaussian processes.  *Annals of Applied Statistics, just accepted.* arXiv:2308.04420
* Barnett, S., Beesley L. J., Booth, A. S., Gramacy, R. B., & Osthus, D. (2024). Monotonic warpings for additive and deep Gaussian processes. *In review.* arXiv:2308.01540

## Folder Organization

* **active_learning:** contains the work from "Active Learning for deep Gaussian process surrogates"
* **contour_location:** contains the work from "Contour location for reliability in airfoil simulation experiments using deep Gaussian processes"
* **misc:** additional examples and demonstrations
* **monoGP:** contains the work from "Monotonic warpings for additive and deep Gaussian processes"
* **satellite_bakeoff**: contains the simulation study from "Non-stationary Gaussian process surrogates"
* **vecchia:** contains the work from "Vecchia-approximated deep Gaussian processes for computer experiments"

## Bibtex References

```
@phdthesis{sauer2023deep,
           title = {Deep Gaussian process surrogates for computer experiments},
           author = {Annie Sauer},
           school = {Virginia Polytechnic Institute and State University},
           year = {2023}
}

@article{sauer2023active,
         title = {Active learning for deep Gaussian process surrogates}, 
         author = {Annie Sauer and Robert B. Gramacy and David Higdon},
         journal = {Technometrics},
         volume = {65},
         numer = {1},
         pages = {4--18},
         year = {2023},
         publisher = {Taylor \& Francis}
}

@article{sauer2023vecchia,
         title = {Vecchia-approximated deep Gaussian processes for computer experiments},
         author = {Annie Sauer and Andrew Cooper and Robert B. Gramacy},
         journal = {Journal of Computational and Graphical Statistics},
         volume = {32},
         number = {3},
         pages = {824--837},
         year = {2023},
         publisher = {Taylor \& Francis}
}

@article{sauer2023nonstationary,
         title = {Non-stationary Gaussian process surrogates},
         author = {Annie Sauer and Andrew Cooper and Robert B. Gramacy},
         year = {2023},
         journal = {arXiv:2305.19242}
}

@article{booth2024contour,
         title = {Contour location for reliability in airfoil simulation experiments using deep Gaussian processes},
         author = {Annie S. Booth and S. Ashwin Renganathan and Robert B. Gramacy},
         year = {2024},
         journal = {Annals of Applied Statistics, just accepted}, 
         url = {arXiv:2308.04420}
}

@article{barnett2024monotonic,
         title = {Monotonic warpings for additive and deep Gaussian processes},
         author = {Steven D. Barnett and Lauren J. Beesley and Annie S. Booth and Robert B. Gramacy and Dave Osthus},
         year = {2024},
         journal = {arXiv:2408.01540}
}

@manual{deepgp,
        title = {deepgp: Bayesian deep Gaussian processes using MCMC},
        author = {Annie S. Booth},
        year = {2023},
        note = {R package version 1.1.3}
}
```
