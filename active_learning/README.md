
# Active Learning Examples

This directory contains work from the following paper:

Sauer, A., Gramacy, R. B., & Higdon, D. (2023).  Active learning for deep Gaussian process surrogates.  *Technometrics,* 65(1), 4-18.  *arXiv:2012.08015*

* **oneD**: code for a one dimensional piecewise function
* **twoD**: code for a two-dimensional exponential function
* **rocket**: code for the LGBB rocket booster simulator
* **satellite**: code for the satellite drag simulator
* **plots**: code for re-producing plots of Sauer, Gramacy, and Higdon (2021)