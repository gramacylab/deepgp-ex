
# One Dimensional Example

* **collect.R**: gathers "RData" files and saves combined RMSE/SCORE in "csv" files
* **oneD.R**: runs sequential design
* **results**: contains "csv" files of results
* **run.sh**: bash script to run multiple reps of the sequential design in parallel
* **run_ARC.sh**: bash script to run sequential design on VT ARC
* **tgp**: contains scripts to run `tgp` comparator