#!/bin/bash
#SBATCH -N4 --ntasks-per-node=25
#SBATCH -t 20:00
#SBATCH -p normal_q
#SBATCH -A deepgp

cd $SLURM_SUBMIT_DIR

module purge
module load parallel
module load intel/18.2 mkl R/3.6.2

# set r libraries
export R_LIBS="$HOME/cascades/R/$rver/intel/18.2/lib:$R_LIBS"

# number of cores used by each R process (1)
export MKL_NUM_THREADS=1

# processes to run at a time (equal to ntasks per node)
pernode=$SLURM_CPUS_ON_NODE

# total number of reps
reps=$(( $pernode * $SLURM_NNODES ))

# get all the node information
scontrol show hostname $SLURM_JOB_NODELIST > ./node_list

# run script in parallel (create temp directory for each rep)
seq 1 $reps | parallel -j$pernode \
      --sshloginfile node_list \
      --workdir $SLURM_SUBMIT_DIR \
      --env R_LIBS \
      "module purge; module load parallel intel/18.2 mkl R/3.6.2; 
      export MKL_NUM_THREADS=1;
      mkdir running_{};
      cd running_{}; 
      R CMD BATCH \"--args seed={}\" ../oneD_tgp.R ../{}.Rout;
      cd ..;      
      rm -r running_{}"

