
# LGBB Rocket Booster Example

* **collect.R**: gathers "RData" files and saves combined RMSE in "csv" files
* **continue.R**: uses an incomplete "RData" file to continue sequential design
* **lgbb_fill.RData**: TGP-surrogate evaluations over dense grid
* **results**: contains "csv" files of results
* **rocket.R**: runs sequential design
* **run.sh**: bash script to run multiple reps of the sequential design in parallel
* **run_ARC.sh**: bash script to run sequential design on VT ARC
* **static.R**: evaluates models on random 300-point static design
* **tgp**: contains files to run (and continue) TGP sequential design
