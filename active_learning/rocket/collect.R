
### Select files

files <- list()
files[[1]] <- list.files("./RData", pattern = "side_layer1_seed[0-9]*.RData")
files[[2]] <- list.files("./RData", pattern = "side_layer2_seed[0-9]*.RData")
files[[3]] <- list.files("./RData", pattern = "side_layer3_seed[0-9]*.RData")
files[[4]] <- list.files("./RData", pattern = "tgp_seed[0-9]*.RData")
layers <- c('layer1', 'layer2', 'layer3', 'tgp')

n <- 300

### Collect and save RMSE

for (j in 1:4) {
  
  k <- length(files[[j]])
  rmse <- matrix(nrow = n, ncol = k)

  for (i in 1:k) {
    load(paste0("RData/", files[[j]][i]))
    rmse[, i] <- rmse_store
  }
  
  write.csv(rmse, paste0('results/side_rmse_', layers[j], '.csv'))
}

### Define function to calculate proportion of points in each region

calc_p <- function(x) {
  low <- mean(x[,1] < 0.333)
  high <- 1 - low
  return(c(low, high))
}

### Specify data frame to store proportions

props <- data.frame(matrix(NA, nrow = k, ncol = 8))
colnames(props) <- c('1_low','1_high','2_low','2_high','3_low','3_high',
                     'tgp_low','tgp_high')

### Calculate and save proportions

for (i in 1:k) {
  load(paste0("RData/",files[[1]][i]))
  props[i, 1:2] <- calc_p(fit$x)
  
  load(paste0("RData/",files[[2]][i]))
  props[i, 3:4] <- calc_p(fit$x)
  
  load(paste0("RData/",files[[3]][i]))
  props[i, 5:6] <- calc_p(fit$x)
  
  load(paste0("RData/",files[[4]][i]))
  props[i, 7:8] <- calc_p(fit$X)
}

write.csv(props, paste0('results/props.csv'))



