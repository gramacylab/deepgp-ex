
### Load libraries

rm(list = ls()) # clear workspace
library(deepgp)

### Set command line variables

seed <- 1
layers <- 1 # 1, 2, or 3
cores <- 1

### Read command line arguments

args <- commandArgs(TRUE)
if(length(args) > 0) 
  for(i in 1:length(args)) 
    eval(parse(text = args[[i]]))
 
cat("seed is ", seed, "\n")
set.seed(seed)
cat("layers is ", layers, "\n")
cat("cores is ", cores, "\n")

### Load and scale data

load('lgbb_data/lgbb_fill.RData')
lgbb.fill$mach <- (lgbb.fill$mach - min(lgbb.fill$mach))/(max(lgbb.fill$mach) - min(lgbb.fill$mach))
lgbb.fill$alpha <- (lgbb.fill$alpha - min(lgbb.fill$alpha))/(max(lgbb.fill$alpha) - min(lgbb.fill$alpha))
lgbb.fill$beta <- (lgbb.fill$beta - min(lgbb.fill$beta))/(max(lgbb.fill$beta) - min(lgbb.fill$beta))
lgbb.fill$side <- (lgbb.fill$side - mean(lgbb.fill$side))/(max(lgbb.fill$side) - min(lgbb.fill$side))
# deterministic model does not require estimation of nugget

### Sample original data

n <- 50 # number of points in starting design
new_n <- 250 # number of points to add sequentially
indx_in <- sample(1:nrow(lgbb.fill), n, replace = F) # indices of original data set

x <- as.matrix(lgbb.fill[indx_in, 1:3])
y <- lgbb.fill$side[indx_in]

### Set initial values for MCMC (function defaults)

if (layers == 1) {
  theta_0 <- 0.5
} else if (layers == 2) {
  theta_y_0 <- 0.5
  theta_w_0 <- c(1, 1, 1)
  w_0 <- x 
} else if (layers == 3) {
  theta_y_0 <- 0.5
  theta_w_0 <- c(1, 1, 1)
  theta_z_0 <- c(1, 1, 1)
  w_0 <- x
  z_0 <- x
}

### Set locations to store results

time_store <- rep(NA, times = n + new_n)
rmse_store <- rep(NA, times = n + new_n)

### Conduct sequential design

for (t in n:(n + new_n)) {
  
  cat('Running ', t,' out of ', n + new_n,'\n')
  
  # Sample candidate/testing set, do not allow duplicates
  if (t %% 10 == 0) m <- 1000 else m <- 500 # fewer points needed when RMSE not calculated
  indx_out <- c(1:nrow(lgbb.fill))[-indx_in] # points that are not yet in the data
  test <- sample(indx_out, m, replace = F)
  xx <- as.matrix(lgbb.fill[test, 1:3])
  yy <- lgbb.fill$side[test]
  
  if (t == n) { # run more iterations the first time
    nmcmc <- 10000
    burn <- 8000
    thin <- 2
  } else {
    nmcmc <- 3000
    burn <- 1000
    thin <- 2
  }
  
  # Fit Model
  if (layers == 1) {
    fit <- fit_one_layer(x, y, nmcmc = nmcmc, theta_0 = theta_0, true_g = 1e-8)
  } else if (layers == 2) {
    fit <- fit_two_layer(x, y, D = 3, nmcmc = nmcmc, theta_y_0 = theta_y_0,
                        theta_w_0 = theta_w_0, w_0 = w_0, true_g = 1e-8)
  } else if (layers == 3) {
    fit <- fit_three_layer(x, y, D = 3, nmcmc = nmcmc, theta_y_0 = theta_y_0,
                          theta_w_0 = theta_w_0, theta_z_0 = theta_z_0, w_0 = w_0,
                          z_0 = z_0, true_g = 1e-8)
  }
  
  # Trim, predict, and calculate ALC 
  fit <- trim(fit, burn, thin = thin)
  if (t %% 10 == 0) { # only predict every 10th point
    fit <- predict(fit, xx, cores = cores)
    alc_list <- ALC(fit, cores = cores)
    rmse_store[t] <- rmse(yy, fit$mean)
  } else { # evaluate ALC only
    alc_list <- ALC(fit, xx, cores = cores)
  }
  
  # Store metrics
  alc <- alc_list$value
  time <- fit$time + alc_list$time
  time_store[t] <- time

  if (t == n + new_n) break
  
  # Select next design point and add to data set
  indx_in <- c(indx_in, test[which.max(alc)])
  x <- rbind(x, xx[which.max(alc),])
  y <- c(y, yy[which.max(alc)])
  
  # Adjust starting locations for the next iteration
  if (layers == 1) {
    theta_0 <- fit$theta[fit$nmcmc]
  } else if (layers == 2 | layers == 3) {
    theta_y_0 <- fit$theta_y[fit$nmcmc]
    theta_w_0 <- fit$theta_w[fit$nmcmc,]
    w_0 <- fit$w[[fit$nmcmc]]
  }
  if (layers == 3) {
    theta_z_0 <- fit$theta_z[fit$nmcmc,]
    z_0 <- fit$z[[fit$nmcmc]]
  }
  
  save(fit, indx_in, time_store, rmse_store, 
       file = paste0("RData/side_layer",layers,"_seed",seed,".RData"))
}

save(fit, indx_in, time_store, rmse_store,
     file = paste0("RData/side_layer",layers,"_seed",seed,".RData"))


