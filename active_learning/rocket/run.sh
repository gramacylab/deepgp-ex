#!/bin/bash

# Read in layers
layers=$1

# Read in cores
cores=$2

# Read in reps
reps=$3

# Run R script in parallel
echo "Running $reps repetitions of $layers layers"
for (( value=1; value<=$reps; value++ ))
do
  R CMD BATCH "--args seed=$value layers=$layers cores=$cores" rocket.R $value.Rout &
done
