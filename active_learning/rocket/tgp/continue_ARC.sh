#!/bin/bash
#SBATCH -N3 --ntasks-per-node=10
#SBATCH -t 24:00:00
#SBATCH -p normal_q
#SBATCH -A deepgp

cd $SLURM_SUBMIT_DIR

module purge
module load parallel
module load intel/18.2 mkl R/3.6.2

# set r libraries
export R_LIBS="$HOME/cascades/R/$rver/intel/18.2/lib:$R_LIBS"

# number of cores used by each R process
export MKL_NUM_THREADS=1

# processes to run at a time
pernode=$(( $SLURM_CPUS_ON_NODE / $MKL_NUM_THREADS ))

# total number of reps
reps=$(( $pernode * $SLURM_NNODES ))

# get all the node information
scontrol show hostname $SLURM_JOB_NODELIST > ./node_list

seq 1 $reps | parallel -j$pernode \
--sshloginfile node_list \
--workdir $SLURM_SUBMIT_DIR \
--env R_LIBS \
"module purge; module load parallel intel/18.2 mkl R/3.6.2; 
      export MKL_NUM_THREADS=1; 
      R CMD BATCH \"--args seed={}\" continue_tgp.R {}.Rout"




