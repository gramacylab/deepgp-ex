
# Satellite Drag Example

This code relies on the public repository, <https://bitbucket.org/gramacylab/tpm>.  To 
use this code as-is, clone this repository in the home directory and rename it `tpm-git`.  
Compile C code by running `R CMD SHLIB -o tpm.so *.c` in the `tpm-git/tpm/src`
directory.

* **collect.R**: gathers "RData" files and saves combined RMSE in "csv" files
* **continue.R**: uses an incomplete "RData" file to continue sequential design
* **initial.csv**: initial design of 110 evaluations
* **results**: contains "csv" files of results
* **run.sh**: bash script to run sequential design (manually adjusted)
* **satellite.R**: runs sequential design
* **sequential_designs**: contains the resulting sequential designs for each model and each replicate
* **set_up.R**: contains mesh/mole settings and functions to scale data
* **test.csv**: 1000-point testing set
* **testing_set.R**: code to randomly select and generate testing set
* **tgp**: contains files to run (and continue) TGP sequential design
