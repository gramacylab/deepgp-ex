
files <- list.files("../RData", pattern = "GRACE_tgp_seed[0-9]*.RData")

k <- length(files)
n <- 500

rmspe <- matrix(nrow = n, ncol = k)

for (i in 1:k) {
  load(paste0("../RData/", files[i]))
  rmspe[, i] <- rmspe_store[1:n]
}

write.csv(rmspe, "../results/GRACE_rmspe_tgp.csv")