#!/bin/bash

# Read in number of layers
layers=$1

# Read in number of reps
reps=$2

# Run R script in parallel
echo "Running $reps repetitions of $layers layers in parallel"
for (( value=1; value<= $reps; value++ ))
do
	R CMD BATCH "--args seed=$value layers=$layers" twoD.R $value.Rout &
done
