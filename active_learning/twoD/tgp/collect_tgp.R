
files <- list.files("../RData", pattern = "tgp_seed[0-9]*.RData")

n <- 100
k <- length(files)

rmse <- matrix(nrow = n, ncol = k)
score <- matrix(nrow = n, ncol = k)

for (i in 1:k) {
    load(paste0("../RData/", files[i]))
    rmse[, i] <- rmse_store
    score[, i] <- score_store
}

write.csv(rmse, "../results/rmse_2D_tgp.csv")
write.csv(score, "../results/score_2D_tgp.csv")

calc_p <- function(x) {
    inside <- mean((x[, 1] < 0.6 & x[, 2] < 0.6))
    outside <- 1 - inside
    return(c(inside, outside))
}

props <- data.frame(matrix(NA, nrow = k, ncol = 2))
colnames(props) <- c("in", "out")

for (i in 1:k) {
    load(paste0("../RData/", files[i]))
    props[i, ] <- calc_p(x)
}

write.csv(props, "../results/props_2D_tgp.csv")
