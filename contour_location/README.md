
# Contour Location Examples

This directory contains work from the following papers:

Booth, A. S., Renganathan S. A., & Gramacy, R. B. (2023).  Contour location for reliability in airfoil simulation experiments using deep Gaussian processes.  *In review.* arXiv:2308.04420

Booth A. S., Renganathan S. A., & Gramacy R. B. (2024).  Actively learning deep Gaussian process models for failure contour and probability estimation.  *AIAA Scitech Forum.*

* **plateau2**: code for the two-dimensional plateau function
* **plateau5**: code for the five-dimensional plateau function
* **airfoil7**: code for the seven-dimensional SU2 airfoil computer experiment
* **tray2**: code for the two-dimensional cross-in-tray function
* **turbine**: code for the two-dimensional turbine simulator
* **dgpPYTORCH.py**: wrapper for the pytorch DGP (used in all static sims)
* **figures.R**: contains R code to generate additional figures from the paper (code for the figures displaying simulation results is available in the respective simulation folders)
* **gmm_functions.R**: contains wrapper functions for a Gaussian mixture model object from the `mclust` package
* **pareto_demo.R**: short example of identifying the Pareto front for two variables
* **tricands.py**: contains python functions to calculate tricands
* **tricands.R**: contains R functions to calculate tricands and Pareto front
