
# SU2 7d Airfoil Simulation

This folder contains work with a 7-dimensional [SU2 airfoil experiment](https://su2code.github.io).

I recommend creating a unique python virtual environment.  The specifics of my python environment are provided in the "requirements.txt" file.  Use `pip install -r requirements.txt` to copy it.  The primary required packages are `pandas` and `numpy`.

To install, clone the SU2 repo: https://github.com/su2code/SU2

And follow the instructions at: https://su2code.github.io/docs_v7/Build-SU2-Linux-MacOS/

For my machine, I did the following:

```
pip install meson
pip install ninja
```
Then navigate to the SU2 repo and run:
```
meson build -Dcustom-mpi=true
ninja -C build install
```
Then add the following to `.bashrc`:
```
export CC=mpicc.mpich
export CXX=mpicxx.mpich
export SU2_RUN="/usr/local/bin"
export SU2_HOME="/home/asauer3/SU2"  # CHANGE TO YOUR LOCAL PATH
export PATH=$PATH::$SU2_RUN
export PYTHONPATH=$SU2_RUN:$PYTHONPATH
```

This folder is organized as follows:

* **data**: contains a static LHS design of size 5000, and `R` code for generating training/testing partitions
* **plots**: contains `R` scripts for collecting saved predictions, computing evaluation metrics, and plotting results
* **run_simulator**: contains all files needed to run the SU2 simulator, most importantly the `python` wrapper
* **seq_design**: contains scripts and results for contour location sequential designs
* **static**: contains scripts and results for static surrogate fits to subsets of the 5000-point LHS
* **requirements.txt**: specifics of the python virtual environment I used to run the simulator
* **scaling_functions.R**: functions for pre-scaling data, used by scripts in the "static" and seq_design" folders
