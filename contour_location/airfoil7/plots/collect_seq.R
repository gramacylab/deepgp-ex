
# Calculates RMSE, CRPS, SENS, SPEC, F1 score for sequential designs

library(deepgp)
limit <- 3 # y < limit = fail
N <- seq(100, 500, by = 10)

for (seed in 1:10) {
  results <- data.frame(matrix(NA, ncol = 6, nrow = 1))
  colnames(results) <- c("N", "RMSE", "CRPS", "SENS", "SPEC", "F1")
  for (n in N) {
    file <- paste0("../seq_design/pred_seed", seed, "/dgp_N", n, ".csv")
    pred <- read.csv(file)
    RMSE <- rmse(pred$yp, pred$mean)
    CRPS <- crps(pred$yp, pred$mean, pred$s2)
    t <- table(pred$yp < limit, pred$mean < limit)
    tn <- t[1, 1] # true pass
    tp <- t[2, 2] # true fail
    fp <- t[1, 2] # predicted fail, actually pass
    fn <- t[2, 1] # predicted pass, actually fail
    SENS <- tp / (fn + tp)
    SPEC <- tn / (tn + fp)
    F1 <- 2 * tp / (2 * tp + fp + fn)
    results <- rbind(results, c(n, RMSE, CRPS, SENS, SPEC, F1))
  }

  results <- results[-1, ]
  write.csv(results, paste0("seq_results_seed", seed, ".csv"), row.names = FALSE)
}

