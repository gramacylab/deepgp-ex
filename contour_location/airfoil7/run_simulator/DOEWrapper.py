
import fileinput
import sys
import pandas as pd
import numpy as np
import subprocess

class DOEWrapper():

    def __init__(self, ):

        self.input_file = None
        self.cfg_file = None

        return

    def read_input_file(self, input_file, header='infer'):

        X = pd.read_csv(input_file, header=header)
        if header:
            print(f'input file headers {X.keys()}')

        return X.values

    def write_cfg_file(self, cfg_file, inputs):
        '''
        function to modify scalar-valued variables in cfg file

        :param cfg_file:
        :param inputs:
        :return:
        '''

        assert type(inputs) is dict, "inputs must be a dictionary"

        for key in inputs.keys():

            for line in fileinput.input(cfg_file, inplace=1):
                sline = line.strip().split("=")

                if sline[0].startswith(key):
                    sline[1] = str(inputs[key])  # str(dv_value)
                    newline = '='.join(sline)
                    nline = line.replace(line.strip(), newline)
                    sys.stdout.write(nline)
                else:

                    print(line, end="")
        return

    def write_DVs_to_cfgfile(self, cfg_file, dv_value):
        """
            Functionality: reads a SU2 config file (cfg_file) and updates the
            design variables with design variables contained in dv_value

        dv_value: -array of design variable values.

                  -first 38 elements-- Hicks & Henne coefficients

                  -last element-- Iterations
        """

        shape_coeffs = dv_value

        for line in fileinput.input(cfg_file, inplace=1):
            sline = line.strip().split("=")

            if sline[0].startswith("DV_VALUE"):
                sline[1] = ','.join([str(i) for i in shape_coeffs])  # str(dv_value)
                newline = '='.join(sline)
                nline = line.replace(line.strip(), newline)
                sys.stdout.write(nline)
            else:
                print(line, end="")

    def run_SU2(self, cfg_file, nprocs=1):
        subprocess.run(["mpirun", "-np", str(nprocs), "SU2_CFD", cfg_file])

    def run_SU2_DEF(self, cfg_file, nprocs=1):
        subprocess.run(["mpirun", "-np", str(nprocs), "SU2_DEF", cfg_file])

    def aero_coeff(self, hist_file):
        hist_primal = pd.read_csv(hist_file)
        CD, CL, CMz = hist_primal['       "CD"       '].values[-1], \
                      hist_primal['       "CL"       '].values[-1], \
                      hist_primal['       "CMz"      '].values[-1]
        return CD, CL, CMz

    def save_to_npfile(self, np_filename, vlist):
        np.save(np_filename, np.array(vlist))

