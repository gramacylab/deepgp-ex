
'''
Evaluates the SU2 freestream solver at a matrix of input locations.
Writes result to separate file.

Requires:
    - install SU2 simulator with MPI parallelization
    
Multiple inputs are run SERIALLY in a for loop, because we rely on 
writing/reading local files.
'''

import os
import subprocess
import pandas as pd
from DOEWrapper import DOEWrapper
import numpy as np
# import time

input_file       = "X.csv" 
output_file      = "Y.csv" 
meshdef_cfg_file = "ffd_rae_.cfg"
primal_cfg_file  = "ffd_rae_deformed_forward.cfg"
start_iter       = 0
nprocs           = 8 # segmentation error may occur for some values, works with 1/8/16/24
hist_file        = "history.csv"

# tic = time.time()

dw = DOEWrapper()
X  = dw.read_input_file(input_file)
n, d = X.shape
y = np.zeros(n)

for i in range(n):

    ###### FREESTREAM VALUES
    # Grab the last three inputs 
    # Write them to the deformed cfg file
    inputs = {'AOA': X[i, -3], 'REYNOLDS_NUMBER': X[i, -2],  'MACH_NUMBER':X[i, -1]}
    dw.write_cfg_file(primal_cfg_file, inputs)
    
    ###### FFD CONTROL POINTS
    # Grab the first four inputs 
    # Write them to the mesh cfg file
    # Create deformed mesh and copy to mesh_deformed.su2
    dv_value = X[i, :-3]
    dw.write_DVs_to_cfgfile(meshdef_cfg_file, dv_value)
    dw.run_SU2_DEF(meshdef_cfg_file, nprocs = nprocs)
    subprocess.run(["cp", "mesh_out.su2", "mesh_deformed.su2"])

    ###### RUN SU2 PRIMAL SOLVER
    dw.write_DVs_to_cfgfile(primal_cfg_file, dv_value) 
    dw.run_SU2(primal_cfg_file, nprocs = nprocs)
    
    ###### EXTRACT AND SAVE RESULTS
    # Do we need to check for convergence?
    CD, CL, CMz = dw.aero_coeff(hist_file)
    LD = CL / CD
    y[i] = LD

    Y = pd.DataFrame({"L.D": y})
    Y.to_csv(output_file, index = False)

# toc = time.time()

###### REMOVE TEMPORARY FILES
temp_files = ["boundary.dat", "ffd_boxes_0.vtk", 
              "ffd_boxes_def_0.vtk", "surface_deformed.csv",
              "surface_deformed.vtu", "volume_deformed.vtu"]
for i in range(len(temp_files)):
    if os.path.isfile(temp_files[i]):
        os.remove(temp_files[i])
