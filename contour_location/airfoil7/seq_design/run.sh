#!/bin/bash

# Must activate correct python environment

seed=1

# Run initial fit, write input to file
echo "Running initial surrogate fit"
R CMD BATCH "--args seed=$seed" initial_fit.R initial.Rout &
wait
echo "Initial surrogate fit done"

for (( acq=2; acq<=401; acq++ ))
do
  echo "Running acquisition "
  echo $acq

  # Evaluate simulator at new input
  echo "Evaluating simulator"
  cd ../run_simulator/
  python run_simulator.py &
  wait
  echo "Simulator done"

  # Update surrogate and select new point
  echo "Updating the surrogate"
  cd ../seq_design/
  R CMD BATCH "--args seed=$seed" update_fit.R &
  wait
  echo "Surrogate done"
done
