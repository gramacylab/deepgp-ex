
# Start with last samples from DGP
# Update DGP with latest acquisition
# Store prediction metrics only every 10th acquisition
# Use tricands/pareto to select next location
# Write next location to "../run_simulator/X.csv" file

library(deepgp)
library(laGP)
library(geometry)
source("../scaling_functions.R")
source("../../tricands.R")

# Read command line arguments -------------------------------------------------

seed <- 1
args <- commandArgs(TRUE)
if(length(args) > 0) 
  for(i in 1:length(args)) 
    eval(parse(text = args[[i]]))
cat("seed is ", seed)
set.seed(seed)

# Read data, get train/test split ---------------------------------------------

dat <- read.csv("../data/freestream_7D_5000.csv")
inputs <- c("X1", "X2", "X3", "X4", "Angle.of.Attack",
            "Reynolds.number", "Mach.number")
output <- "L.D"
d <- 7
limit <- 3

design <- read.csv(paste0("designs/dgp_seed", seed, ".csv"))
new_x <- read.csv("../run_simulator/X.csv")
new_y <- read.csv("../run_simulator/Y.csv")[["L.D"]]
x <- as.matrix(rbind(design[inputs], new_x))
y <- c(design[[output]], new_y)

# Fit DGP ---------------------------------------------------------------------

# Scale inputs and response
x <- scale_down(x)
y_mean <- mean(y)
y_sd <- sd(y)
y <- (y - y_mean) / y_sd
limit <- (limit - y_mean) / y_sd

# Fit separable GP for pre-scaling
gp <- newGPsep(x, y, d = 0.1, g = 1e-6, dK = TRUE)
mle <- mleGPsep(gp, param = "d", tmin = 1e-8, tmax = 10)
theta <- sqrt(mle$d)
deleteGPsep(gp)

# Pre-scale inputs by estimated lengthscales  
x <- pre_scale(x, theta)

# Fit two-layer DGP, starting where we left off
load(paste0("RData/last_sample_seed", seed, ".RData"))
# Note - these are not scaled exactly right, but they are a starting point
fit <- fit_two_layer(x, y, nmcmc = 1000, true_g = 1e-6,
                     theta_y_0 = last_sample$theta_y,
                     theta_w_0 = last_sample$theta_w,
                     w_0 = last_sample$w)
fit <- trim(fit, 500)

# Predict in batches every 10th acquisition
if (nrow(x) %% 10 == 0) {
  
  test <- read.csv(paste0("../data/test_seed", seed, ".csv"))[[1]]
  xp <- as.matrix(dat[inputs][test, ])
  yp <- dat[[output]][test]
  xp <- scale_down(xp)
  xp <- pre_scale(xp, theta)
  np <- nrow(xp)
  
  pred_store <- data.frame(yp = yp,
                           mean = rep(NA, times = np),
                           s2 = rep(NA, times = np))
  chunks <- split(1:np, cut(seq_along(1:np), 9, labels = FALSE))
  for (i in 1:length(chunks)) {
    p <- predict(fit, xp[chunks[[i]], ], lite = TRUE, cores = 4)
    p$mean <- (p$mean * y_sd) + y_mean # Unscale
    p$s2 <- (p$s2 * y_sd^2) # Unscale
    pred_store$mean[chunks[[i]]] <- p$mean
    pred_store$s2[chunks[[i]]] <- p$s2
  }
  write.csv(pred_store, paste0("pred_seed", seed, "/dgp_N", nrow(x), ".csv"),
            row.names = F)
}

# Select next design point ----------------------------------------------------

# Calculate tricands in the UN-PRE-SCALED space ([0, 1]^d bounding box)
o <- order(abs(y - limit), decreasing = FALSE)
xcand <- tricands(unpre_scale(x, theta), p = 0.9, max = 100 * d, 
                  ordering = o)

# Convert candidates to pre-scaled version before predicting
xcand <- pre_scale(xcand, theta)

# Evaluate entropy and predictive standard deviation
pcand <- predict(fit, xcand, lite = TRUE, cores = 4)
s <- sqrt(pcand$s2)
fail_prob <- pnorm((pcand$mean - limit) / s)
ent <- -(1 - fail_prob) * log(1 - fail_prob) - fail_prob * log(fail_prob)
ent[which(is.nan(ent))] <- 0

# Choose point on pareto front
inds <- get_pareto(ent, s)
if (length(inds) == 1) ru <- inds else ru <- sample(inds, 1)
new_x <- matrix(xcand[ru, ], nrow = 1)

# Convert back to original scale, write to file
new_x <- scale_up(unpre_scale(new_x, theta))
colnames(new_x) <- colnames(x)
write.csv(new_x, "../run_simulator/X.csv", row.names = FALSE)

# Save input design to file
x <- scale_up(unpre_scale(x, theta))
y <- y * y_sd + y_mean
design <- cbind(x, y)
colnames(design) <- c(inputs, output)
write.csv(design, paste0("designs/dgp_seed", seed, ".csv"), row.names = FALSE)

# Save fit
last_sample <- list("theta_y" = fit$theta_y[fit$nmcmc],
                    "theta_w" = fit$theta_w[fit$nmcmc, ],
                    "w" = fit$w[[fit$nmcmc]])
save(last_sample, file = paste0("RData/last_sample_seed", seed, ".RData"))
