
# Fits a pytorch DGP to a subset of 500 points, predicts at remaining 4500
# Writes point-wise predictive mean and variance to csv file

import torch
import tqdm
import gpytorch
from gpytorch.means import ConstantMean, LinearMean
from gpytorch.kernels import RBFKernel, ScaleKernel, MaternKernel
from gpytorch.variational import VariationalStrategy, CholeskyVariationalDistribution
from gpytorch.distributions import MultivariateNormal
from gpytorch.models import ApproximateGP, GP
from gpytorch.mlls import VariationalELBO, AddedLossTerm
from gpytorch.likelihoods import GaussianLikelihood
from gpytorch.models.deep_gps import DeepGPLayer, DeepGP
from gpytorch.mlls import DeepApproximateMLL
import urllib.request
import os
from scipy.io import loadmat
from math import floor
import gpytorch
import math
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from botorch.utils.transforms import normalize, unnormalize, standardize
import properscoring as ps
from time import time
from mpi4py import MPI

smoke_test = ('CI' in os.environ)
def unstandardize(Y_actual, Y):
    std_ = Y_actual.squeeze().std()
    mean_ = Y_actual.squeeze().mean()
    return Y * std_ + mean_

Data = pd.read_csv('../data/freestream_7D_5000.csv')
X = Data.values[...,:7]
Y = Data.values[...,-1]
bounds = torch.tensor(np.stack((X.min(axis=0), X.max(axis=0))), dtype=torch.float32)
X = torch.tensor(X, dtype=torch.float32)#[:1500]
Y = torch.tensor(Y, dtype=torch.float32)#[:1500]
X_normalized = normalize(X, bounds)
Y_standardized = standardize(Y)

class DeepGPHiddenLayer(DeepGPLayer):
    '''
    This class defines the GP prior on a hidden layer. It also defines the variational
    strategy for inference.
    '''
    def __init__(self, input_dims, output_dims, num_inducing=128, mean_type='constant'):
        if output_dims is None:
            inducing_points = torch.randn(num_inducing, input_dims)
            batch_shape = torch.Size([])
        else:
            inducing_points = torch.randn(output_dims, num_inducing, input_dims)
            batch_shape = torch.Size([output_dims])

        variational_distribution = CholeskyVariationalDistribution(
            num_inducing_points=num_inducing,
            batch_shape=batch_shape
        )

        variational_strategy = VariationalStrategy(
            self,
            inducing_points,
            variational_distribution,
            learn_inducing_locations=True
        )

        super(DeepGPHiddenLayer, self).__init__(variational_strategy, input_dims, output_dims)

        if mean_type == 'constant':
            self.mean_module = ConstantMean(batch_shape=batch_shape) # default option
        else:
            self.mean_module = LinearMean(input_dims)
        self.covar_module = ScaleKernel(
            MaternKernel(batch_shape=batch_shape, ard_num_dims=input_dims), # Matern kernel choice
            batch_shape=batch_shape, ard_num_dims=None
        )

    def forward(self, x):
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        return MultivariateNormal(mean_x, covar_x)

    def __call__(self, x, *other_inputs, **kwargs):
        """
        Overriding __call__ isn't strictly necessary, but it lets us add concatenation based skip connections
        easily. For example, hidden_layer2(hidden_layer1_outputs, inputs) will pass the concatenation of the first
        hidden layer's outputs and the input data to hidden_layer2.
        """
        if len(other_inputs):
            if isinstance(x, gpytorch.distributions.MultitaskMultivariateNormal):
                x = x.rsample()

            processed_inputs = [
                inp.unsqueeze(0).expand(gpytorch.settings.num_likelihood_samples.value(), *inp.shape)
                for inp in other_inputs
            ]

            x = torch.cat([x] + processed_inputs, dim=-1)

        return super().__call__(x, are_samples=bool(len(other_inputs)))

num_hidden_dims = 1


class DeepGP(DeepGP):
    '''
    This class defines the DGP
    '''
    def __init__(self, train_x_shape):
        hidden_layer = DeepGPHiddenLayer(
            input_dims=train_x_shape[-1],
            output_dims=num_hidden_dims,
            mean_type='constant',
        )

        last_layer = DeepGPHiddenLayer(
            input_dims=hidden_layer.output_dims,
            output_dims=None, #why?
            mean_type='constant',
        )

        super().__init__()

        self.hidden_layer = hidden_layer
        self.last_layer = last_layer
        self.likelihood = GaussianLikelihood()

    def forward(self, inputs):
        hidden_rep1 = self.hidden_layer(inputs)
        output = self.last_layer(hidden_rep1)
        return output

    def predict(self, test_loader):
        with torch.no_grad():
            mus = []
            variances = []
            lls = []
            for x_batch, y_batch in test_loader:
                preds = self.likelihood(self(x_batch))
                mus.append(preds.mean)
                variances.append(preds.variance)
                lls.append(model.likelihood.log_marginal(y_batch, model(x_batch)))

        return torch.cat(mus, dim=-1), torch.cat(variances, dim=-1), torch.cat(lls, dim=-1)

world_comm = MPI.COMM_WORLD
world_size = world_comm.Get_size()
world_rank = world_comm.Get_rank()

seed          = world_rank + 1
rmse_         = []
rmse_uns_     = []
prob_score_   = []
num_epochs    = 2500
num_samples   = 32
learning_rate = 1e-3

batch_sizes = [500]

for batch_size in batch_sizes:
    t1 = time()

    # Read the train and test indices from Annie's data
    test_inds  = pd.read_csv("../data/test_seed" + str(seed) + ".csv")["x"] - 1
    train_inds = np.delete(range(Data.shape[0]), test_inds)
    ntrain = train_inds.shape[0]
    ntest  = test_inds.shape[0]

    train_x = X_normalized[train_inds]
    train_y = Y_standardized[train_inds]
    test_x  = X_normalized[test_inds]
    test_y  = Y_standardized[test_inds]

    from torch.utils.data import TensorDataset, DataLoader

    train_dataset = TensorDataset(train_x, train_y)
    train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)

    model = DeepGP(train_x.shape)
    if torch.cuda.is_available():
        model = model.cuda()

    optimizer = torch.optim.Adam([
        {'params': model.parameters()},
    ], lr=learning_rate)
    mll = DeepApproximateMLL(VariationalELBO(model.likelihood, model, train_x.shape[-2]))

    epochs_iter = tqdm.tqdm_notebook(range(num_epochs), desc="Epoch")
    for i in epochs_iter:
        # Within each iteration, we will go over each minibatch of data
        minibatch_iter = tqdm.notebook.tqdm(train_loader, desc="Minibatch", leave=False)
        for x_batch, y_batch in minibatch_iter:
            with gpytorch.settings.num_likelihood_samples(num_samples):
                optimizer.zero_grad()
                output = model(x_batch)
                loss = -mll(output, y_batch)
                loss.backward()
                optimizer.step()
                print(f'epoch {i} loss {loss.item()}', flush=True)
                minibatch_iter.set_postfix(loss=loss.item())

    test_dataset = TensorDataset(test_x, test_y)
    test_loader = DataLoader(test_dataset, batch_size=batch_size)

    model.eval()
    predictive_means, predictive_variances, test_lls = model.predict(test_loader)

    rmse = torch.mean(torch.pow(predictive_means.mean(0) - test_y, 2)).sqrt()

    mu_pred  = unstandardize(Y, predictive_means.mean(0))
    Y_pred   = unstandardize(Y, test_y)
    rmse_uns = torch.mean(torch.pow(mu_pred - Y_pred, 2)).sqrt()
    print(f"RMSE: {rmse.item()}, NLL: {-test_lls.mean().item()}")
    print(f"RMSE_uns: {rmse_uns.item()}, NLL: {-test_lls.mean().item()}")
    rmse_.append(rmse.item())
    rmse_uns_.append(rmse_uns.item())

    # probability scores
    std_pred = predictive_variances.mean(0).sqrt()
    prob_score = ps.crps_gaussian(0, mu=mu_pred.numpy(), sig=std_pred.numpy() * Y.squeeze().std().numpy())
    prob_score_.append(prob_score)
    print(f'prob score {prob_score.mean()}')
    print(f'time elapsed {time()-t1}')

# Predict and store results                                                                                                                         
mean = mu_pred.numpy()
s2 = std_pred.numpy()**2 * Y.squeeze().std().numpy()**2
pred_store = pd.DataFrame({"yp": Y_pred.numpy(), "mean": mean, "s2": s2})
pred_store.to_csv("predictions/dgppytorch_seed" + str(seed) + ".csv", index = False)
