
# Fits a one-layer GP to a subset of 500 points, predicts at remaining 4500
# Writes point-wise predictive mean and variance to csv file

import pandas as pd
import numpy as np
import sys
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import Matern

# Read command line arguments -------------------------------------------------

#seed = 1
seed = int(sys.argv[1])
np.random.seed(seed)

# Read data, get train/test split ---------------------------------------------

dat = pd.read_csv("../data/freestream_7D_5000.csv")
inputs = ["X1", "X2", "X3", "X4", "Angle.of.Attack",
          "Reynolds.number","Mach.number"]
output = "L.D"
d = 7

test = pd.read_csv("../data/test_seed" + str(seed) + ".csv")["x"] - 1
train = np.delete(range(dat.shape[0]), test)
x = np.array(dat.loc[train, inputs])
y = np.array(dat.loc[train, output])
xp = np.array(dat.loc[test, inputs])
yp = np.array(dat.loc[test, output])

# Fit GP ----------------------------------------------------------------------

# Scale inputs, using only what we know from the training data
bounds = np.asarray(pd.read_csv("../bounds.csv"))
for i in range(d):
    x[:, i] = (x[:, i] - bounds[0, i]) / (bounds[1, i] - bounds[0, i])
    xp[:, i] = (xp[:, i] - bounds[0, i]) / (bounds[1, i] - bounds[0, i])
    
# Scale response, using only what we know from the training data
y_mean = np.mean(y)
y_sd = np.std(y)
y = (y - y_mean) / y_sd    

# Fit separable GP
kernel = 1 * Matern(length_scale = [0.1] * d, length_scale_bounds = (0.001, 10), 
                    nu = 2.5)
gp = GPR(kernel, alpha = 1e-6)
gp.fit(x, y)

# Predict and store results
mean, s = gp.predict(xp, return_std = True)
mean = (mean * y_sd) + y_mean
s2 = s**2 * y_sd**2
pred_store = pd.DataFrame({"yp": yp, "mean": mean, "s2": s2})
pred_store.to_csv("predictions/gpMLE_seed" + str(seed) + ".csv", index = False)

