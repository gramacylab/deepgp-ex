#!/bin/bash

for (( seed=1; seed<=10; seed++ ))
do
   R CMD BATCH "--args seed=$seed" dgpESS.R $seed.Rout &
done
