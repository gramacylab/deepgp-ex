import torch
import tqdm
import gpytorch
from gpytorch.means import ConstantMean, LinearMean
from gpytorch.kernels import RBFKernel, ScaleKernel, MaternKernel
from gpytorch.variational import VariationalStrategy, CholeskyVariationalDistribution
from gpytorch.distributions import MultivariateNormal
from gpytorch.models import ApproximateGP, GP
from gpytorch.mlls import VariationalELBO, AddedLossTerm
from gpytorch.likelihoods import GaussianLikelihood
from botorch.models.gpytorch import GPyTorchModel

from gpytorch.models.deep_gps import DeepGPLayer, DeepGP
from gpytorch.mlls import DeepApproximateMLL
from torch.utils.data import TensorDataset, DataLoader
import urllib.request
import os
from scipy.io import loadmat
from math import floor
import gpytorch
import math
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from botorch.utils.transforms import normalize, unnormalize, standardize
import properscoring as ps
from time import time

class DeepGPHiddenLayer(DeepGPLayer):
    '''
    This class defines the GP prior on a hidden layer. It also defines the variational
    strategy for inference.
    '''
    def __init__(self, input_dims, output_dims, num_inducing=32, mean_type='constant'):
        if output_dims is None:
            inducing_points = torch.randn(num_inducing, input_dims)
            batch_shape = torch.Size([])
        else:
            inducing_points = torch.randn(output_dims, num_inducing, input_dims)
            batch_shape = torch.Size([output_dims])

        variational_distribution = CholeskyVariationalDistribution(
            num_inducing_points=num_inducing,
            batch_shape=batch_shape
        )

        variational_strategy = VariationalStrategy(
            self,
            inducing_points,
            variational_distribution,
            learn_inducing_locations=True
        )

        super(DeepGPHiddenLayer, self).__init__(variational_strategy, input_dims, output_dims)

        if mean_type == 'constant':
            self.mean_module = ConstantMean(batch_shape=batch_shape) # default option
        else:
            self.mean_module = LinearMean(input_dims)
        self.covar_module = ScaleKernel(
            MaternKernel(batch_shape=batch_shape, ard_num_dims=input_dims), # Matern kernel choice
#            batch_shape=batch_shape, ard_num_dims=None
        )

    def forward(self, x):
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        return MultivariateNormal(mean_x, covar_x)

    def __call__(self, x, *other_inputs, **kwargs):
        """
        Overriding __call__ isn't strictly necessary, but it lets us add concatenation based skip connections
        easily. For example, hidden_layer2(hidden_layer1_outputs, inputs) will pass the concatenation of the first
        hidden layer's outputs and the input data to hidden_layer2.
        """
        if len(other_inputs):
            if isinstance(x, gpytorch.distributions.MultitaskMultivariateNormal):
                x = x.rsample()

            processed_inputs = [
                inp.unsqueeze(0).expand(gpytorch.settings.num_likelihood_samples.value(), *inp.shape)
                for inp in other_inputs
            ]

            x = torch.cat([x] + processed_inputs, dim=-1)

        return super().__call__(x, are_samples=bool(len(other_inputs)))

class DeepGP(DeepGP, GPyTorchModel):
    '''
    This class defines the
    '''

    def __init__(self, train_x_shape, num_hidden_dims=1):
        num_hidden_dims = num_hidden_dims
        hidden_layer = DeepGPHiddenLayer(
            input_dims=train_x_shape[-1],
            output_dims=num_hidden_dims,
            mean_type='constant',
        )
        
#         hidden_layer2 = DeepGPHiddenLayer(
#             input_dims=hidden_layer.output_dims,
#             output_dims=num_hidden_dims,
#             mean_type='constant',
#         )

        last_layer = DeepGPHiddenLayer(
            input_dims=hidden_layer.output_dims,
            output_dims=None,  # why?
            mean_type='constant',
        )

        super().__init__()

        self.hidden_layer = hidden_layer
        self.last_layer = last_layer
        self.likelihood = GaussianLikelihood()
        self._num_outputs = 1

    def forward(self, inputs):
        hidden_rep1 = self.hidden_layer(inputs)
        output = self.last_layer(hidden_rep1)
        return output

    def predict(self, test_loader, model):
        with torch.no_grad():
            mus = []
            variances = []
            lls = []
            for x_batch, y_batch in test_loader:
                preds = self.likelihood(self(x_batch))
                mus.append(preds.mean)
                variances.append(preds.variance)
                lls.append(model.likelihood.log_marginal(y_batch, model(x_batch)))

        return torch.cat(mus, dim=-1), torch.cat(variances, dim=-1), torch.cat(lls, dim=-1)

    def metrics(self, model_, test_x, test_y, Y, batch_size=150):
        ### Prediction on test data
        test_dataset = TensorDataset(test_x, test_y)
        test_loader = DataLoader(test_dataset, batch_size=batch_size)
        model_.eval()
        predictive_means, predictive_variances, test_lls = model_.predict(test_loader, model_)
        self.predictive_means = predictive_means
        self.predictive_variances = predictive_variances
        self.test_lls = test_lls

        self.rmse = torch.mean(torch.pow(predictive_means.mean(0) - test_y, 2)).sqrt()

        self.mu_pred = self.unstandardize(Y, predictive_means.mean(0))
        self.Y_pred  = self.unstandardize(Y, test_y)
        self.rmse_uns= torch.mean(torch.pow(self.mu_pred - self.Y_pred, 2)).sqrt()

        # probability scores
        std_pred = predictive_variances.mean(0).sqrt()
        self.prob_score = ps.crps_gaussian(0, mu=self.mu_pred.numpy(), sig=std_pred.numpy() * Y.squeeze().std().numpy())
        return

    def unstandardize(self, Y_actual, Y):
        std_ = Y_actual.squeeze().std()
        mean_ = Y_actual.squeeze().mean()
        return Y * std_ + mean_

def train_deepGP(model, train_x, train_y,
                 batch_size=1000,
                 num_epochs=100,
                 learning_rate=1e-3,
                 num_samples=128):
    r'''
    :param model: a deep GP model
    :param train_x: full training x
    :param train_y: full training y
    :param batch_size: minibatchsize
    :param num_epochs: number of epochs
    :param learning_rate: learning rate for Adam
    :param num_samples:
    :return:
    '''

    optimizer = torch.optim.Adam([
        {'params': model.parameters()},
    ], lr=learning_rate)
    mll = DeepApproximateMLL(VariationalELBO(model.likelihood, model, train_x.shape[-2]))

    train_dataset = TensorDataset(train_x, train_y)
    train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)

    epochs_iter = tqdm.tqdm_notebook(range(num_epochs), desc="Epoch")
    for i in epochs_iter:
        # Within each iteration, we will go over each minibatch of data
        minibatch_iter = tqdm.notebook.tqdm(train_loader, desc="Minibatch", leave=False)
        for x_batch, y_batch in minibatch_iter:
            with gpytorch.settings.num_likelihood_samples(num_samples):
                model.likelihood.noise_covar.raw_noise_constraint.lower_bound = torch.tensor(0)
                model.likelihood.noise_covar.raw_noise_constraint.upper_bound = torch.tensor(1e-6)
                model.likelihood.noise_covar.raw_noise.requires_grad = False
                optimizer.zero_grad()
                output = model(x_batch)
                loss = -mll(output, y_batch)
                loss.backward()
                optimizer.step()
                print(f'epoch {i} loss {loss.item()}', flush=True)
                minibatch_iter.set_postfix(loss=loss.item())
    return model

