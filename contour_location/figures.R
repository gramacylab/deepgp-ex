
library(deepgp)
library(geometry)
library(RColorBrewer)
source("tricands.R")
col1 <- brewer.pal(3, 'Set1')[2]
col2 <- rgb(col2rgb(col1)[1], col2rgb(col1)[2], col2rgb(col1)[3], 
            max = 255, alpha = 65)

get_pareto <- function(var1, var2) {
  # Order observations based on the first variable
  o <- order(var1, decreasing = TRUE)
  var2 <- var2[o]
  skyline <- c(1)
  # Add lesser rows only if they win in the second variable
  for (i in 2:length(var1)) {
    if (all(var2[i] > var2[(i - 1):1]))
      skyline <- append(skyline, i)
  }
  # Return un-ordered indices
  return(o[skyline])
}

# Plateau function
f <- function(x) {
  x <- x * 4 - 2
  xs <- -4 - 3*apply(x, 1, sum)
  y <- 2 * pnorm(xs * sqrt(2)) - 1
  return(y)
}
limit <- 0

# Training data
set.seed(10)
d <- 2
n0 <- 15
x <- matrix(runif(n0 * d), ncol = d)
y <- f(x)

# Figure 2: one-dimensional slice ---------------------------------------------

x_contour <- 0.46667 # value of x1 on contour for x2 = 0.2

# Testing data for one-dimensional slice
xx <- cbind(seq(0, 1, length = 100), 0.2)
yy <- f(xx)

# Fit one layer, predict in 1d
fit.gp <- fit_one_layer(x, y, nmcmc = 10000, true_g = 1e-6)
fit.gp <- trim(fit.gp, 5000, 5)
pred.gp <- predict(fit.gp, xx, entropy_limit = limit)
ent.gp <- deepgp:::calc_entropy(pred.gp$mean, pred.gp$s2, limit)

# Fit two layer, predict in 1d
fit.dgp <- fit_two_layer(x, y, nmcmc = 10000, true_g = 1e-6)
fit.dgp <- trim(fit.dgp, 5000, 5)
pred.dgp <- predict(fit.dgp, xx, entropy_limit = limit)
ent.dgp <- deepgp:::calc_entropy(pred.dgp$mean, pred.dgp$s2, limit)

#pdf("~/aero-git/papers/figures/oneD.pdf", width = 14, height = 5)
par(mfrow = c(1, 3))

# Left panel: perspective plot of surface
par(mar = c(2, 2, 4, 2))
small_grid <- seq(0, 1, length = 35)
small_xgrid <- as.matrix(expand.grid(small_grid, small_grid))
small_ygrid <- f(small_xgrid)
persp(small_grid, small_grid, matrix(small_ygrid, nrow = length(small_grid)), 
      theta = 60, phi = 30, r = 30, xlab = "", ylab = "", zlab = "y",
      main = "Plateau Function", cex.main = 2.2, cex.lab = 2)

# Middle panel: one-layer GP slice
par(mar = c(5, 4, 4, 2))
q1 <- pred.gp$mean - 1.96*sqrt(pred.gp$s2)
q3 <- pred.gp$mean + 1.96*sqrt(pred.gp$s2)
plot(xx[, 1], pred.gp$mean, col = col1, type = "l", ylim = c(-2.8, 2), 
     xlab = expression("x"[1]), 
     main = expression("One-layer GP (x"[2] * " = 0.2)"), ylab = "y", 
     cex.main = 2.2, cex.lab = 1.7, 
     cex.axis = 1.5, lwd = 3, lty = 2)
polygon(c(xx[, 1], rev(xx[, 1])), c(q1, rev(q3)), col = col2, border = NA)
lines(xx[, 1], q1, col = col1, lty = 2, lwd = 3)
lines(xx[, 1], q3, col = col1, lty = 2, lwd = 3)
lines(xx[, 1], yy, col = "orange", lwd = 3)
lines(xx[, 1], pred.gp$entropy * 1.5 - 2.8, col = 2, lwd = 3, lty = 4)
lines(xx[, 1], ent.gp * 1.5 - 2.8, lwd = 3, lty = 3)
points(x_contour, f(matrix(c(x_contour, 0.2), nrow = 1)), pch = "*", 
       cex = 5, col = "purple")
lines(c(x_contour, x_contour), c(-2.8, -1.5), col = "purple", lwd = 2)

# Right panel: two-layer DGP slice
q1 <- pred.dgp$mean - 1.96*sqrt(pred.dgp$s2)
q3 <- pred.dgp$mean + 1.96*sqrt(pred.dgp$s2)
plot(xx[, 1], pred.dgp$mean, col = col1, type = "l", ylim = c(-2.8, 2), 
     xlab = expression("x"[1]), 
     main = expression("Two-layer DGP (x"[2] * " = 0.2)"), ylab = "y", 
     cex.main = 2.2, cex.lab = 1.7, cex.axis = 1.5, lwd = 3, lty = 2)
polygon(c(xx[, 1], rev(xx[, 1])), c(q1, rev(q3)), col = col2, border = NA)
lines(xx[, 1], q1, col = col1, lty = 2, lwd = 3)
lines(xx[, 1], q3, col = col1, lty = 2, lwd = 3)
lines(xx[, 1], yy, col = "orange", lwd = 3)
lines(xx[, 1], pred.dgp$entropy * 1.5 - 2.8, col = 2, lwd = 3, lty = 4)
lines(xx[, 1], ent.dgp * 1.5 - 2.8, lwd = 3, lty = 3)
points(x_contour, f(matrix(c(x_contour, 0.2), nrow = 1)), pch = "*", 
       cex = 5, col = "purple")
lines(c(x_contour, x_contour), c(-2.8, -1.5), col = "purple", lwd = 2)

#dev.off()

# Save legend as separate figure
#pdf("~/aero-git/papers/figures/oneD_legend.pdf", width = 13, height = 2)
par(mar = c(1, 1, 1, 1), xpd = TRUE)
plot(1, type = "n", axes = F, xlab = "", ylab = "")
text <- c("True Function",  "True Location of Contour    ", 
          "Posterior Mean and 95% CI       ", "", "Entropy", "post hoc Entropy")
legend("topleft", legend = text, ncol = 3,
       inset = 0, col = c("orange", "purple", col1, NA, 2, 1),
       lty = c(1, 1, 2, NA, 4, 3), cex = 1.4, lwd = 3,
       pch = c(NA, 8, NA, NA, NA, NA))
#dev.off()

# Figure 3: DGP in two dimensions ---------------------------------------------

# Testing data for two-dimensions
ng <- 100
np <- ng * ng
chunks <- split(1:np, cut(seq_along(1:np), ceiling(np / 500), labels = FALSE))
g <- seq(0, 1, length = ng)
xgrid <- as.matrix(expand.grid(g, g))
ygrid <- f(xgrid)

# Use previous two layer fit, predict in 2d
mean.dgp <- rep(NA, times = np)
s.dgp <- rep(NA, times = np)
for (i in 1:length(chunks)) {
  p <- predict(fit.dgp, xgrid[chunks[[i]], ], lite = TRUE)
  mean.dgp[chunks[[i]]] <- p$mean
  s.dgp[chunks[[i]]] <- sqrt(p$s2)
}

# Calculate post hoc entropy and pareto front
failprob <- pnorm((mean.dgp - limit) / s.dgp)
ent <- -(1 - failprob) * log(1 - failprob) - failprob * log(failprob)
ent[which(is.nan(ent))] <- 0
inds <- get_pareto(ent, s.dgp)

# Calculate tricands and pareto front
tri <- tricands(x, p = 0.9)
pred.dgp.cand <- predict(fit.dgp, tri, lite = TRUE)
s.dgp.cand <- sqrt(pred.dgp.cand$s2)
failprob <- pnorm((pred.dgp.cand$mean - limit) / s.dgp.cand)
ent.cand <- -(1 - failprob) * log(1 - failprob) - failprob * log(failprob)
ent.cand[which(is.nan(ent.cand))] <- 0
inds.cand <- get_pareto(ent.cand, s.dgp.cand)

# pdf("~/aero-git/papers/figures/plateau1.pdf", width = 11, height = 4)
par(mfrow = c(1, 3), mar = c(5, 5, 4, 2))

# Left panel: DGP predicted mean
image(g, g, matrix(mean.dgp, nrow = ng), col = heat.colors(128),
      xlab = expression("x"[1]), ylab = expression("x"[2]), 
      main = "DGP Predicted Mean", cex.main = 1.8, cex.lab = 1.5)
points(x[, 1], x[, 2])
contour(g, g, matrix(ygrid, nrow = ng), add = TRUE, level = limit,
        col = 4, lty = 1, lwd = 2)
contour(g, g, matrix(mean.dgp, nrow = ng), add = TRUE, level = limit,
        col = 1, lty = 2, lwd = 2)
points(xgrid[inds, , drop = FALSE], col = "purple", pch = 17, cex = 2)
points(tri[inds.cand, , drop = FALSE], pch = 18, col = "green", cex = 2.5)
box()

# Middle panel: DGP predicted standard deviation
image(g, g, matrix(s.dgp, nrow = ng), col = heat.colors(128),
      xlab = expression("x"[1]), ylab = expression("x"[2]), cex.main = 1.8,
      main = "DGP Predicted Std. Dev.", cex.lab = 1.5)
points(x[, 1], x[, 2])
contour(g, g, matrix(ygrid, nrow = ng), add = TRUE, level = limit,
        col = 4, lty = 1, lwd = 2)
contour(g, g, matrix(mean.dgp, nrow = ng), add = TRUE, level = limit,
        col = 1, lty = 2, lwd = 2)
points(xgrid[inds, , drop = FALSE], col = "purple", pch = 17, cex = 2)
points(tri[inds.cand, , drop = FALSE], pch = 18, col = "green", cex = 2.5)
box()

# Right panel: DGP predicted entropy
image(g, g, matrix(ent, nrow = ng), col = heat.colors(128),
      xlab = expression("x"[1]), ylab = expression("x"[2]), 
      main = "DGP Predicted Entropy", cex.main = 1.8, cex.lab = 1.5)
points(x[, 1], x[, 2])
contour(g, g, matrix(ygrid, nrow = ng), add = TRUE, level = limit,
        col = 4, lty = 1, lwd = 2)
contour(g, g, matrix(mean.dgp, nrow = ng), add = TRUE, level = limit,
        col = 1, lty = 2, lwd = 2)
points(xgrid[inds, , drop = FALSE], col = "purple", pch = 17, cex = 2)
points(tri[inds.cand, , drop = FALSE], pch = 18, col = "green", cex = 2.5)
box()

# dev.off()

# Figure 4: tricands and Pareto -----------------------------------------------

# Get tricands pieces
int <- tricands.interior(x)
fr <- tricands.fringe(x, p = 0.9)

# Create Pareto demo
set.seed(7)
n <- 50
a <- rgamma(n, 1, 1)
a <- (a - min(a)) / (max(a) - min(a))
b <- rgamma(n, 1, 1)
b <- (b - min(b)) / (max(b) - min(b))
j <- get_pareto(a, b)

pdf("~/aero-git/papers/figures/plateau2.pdf", width = 11, height = 3.9)
par(mfrow = c(1, 3), mar = c(5, 5, 4, 2))

# Left panel: tricands
plot(x, xlab = expression("x"[1]), ylab = expression("x"[2]), xlim = c(0, 1), 
     ylim = c(0, 1), main = "internal and fringe tricands", cex = 1.5, 
     cex.main = 1.8, cex.lab = 1.5)
for (i in 1:nrow(int$tri))
  lines(x[c(int$tri[i, ], int$tri[i, 1]), ])
points(int$cand, col = 2, pch = 18, cex = 1.5)
arrows(fr$XB[, 1], fr$XB[, 2], fr$XF[, 1], fr$XF[, 2], lty = 2)
points(fr$XF, col = 2, pch = 18, cex = 1.5)
legend(0.71, 0.33, c("design", "triangles", "tricands"),
       pch = c(1, NA, 18), lty = c(NA, 1, NA), ncol = 1,
       col = c(1, 1, 2), cex = 1.1, pt.cex = 1.5)

# Middle panel: Pareto front demonstration
plot(a, b, xlab = "Var 1", ylab = "Var 2", cex.main = 1.8,
     main = "Pareto front demonstration")
o <- order(a[j], decreasing = TRUE)
lines(c(a[j[o[1]]], a[j[o[1]]]), c(0, b[j[o[1]]]), col = 4, lwd = 2, lty = 2)
for (i in 2:length(j)) {
  lines(c(a[j[o[i-1]]], a[j[o[i]]]), c(b[j[o[i-1]]], b[j[o[i-1]]]), 
        col = 4, lwd = 2, lty = 2)
  lines(c(a[j[o[i]]], a[j[o[i]]]), c(b[j[o[i-1]]], b[j[o[i]]]), 
        col = 4, lwd = 2, lty = 2)
}
lines(c(0, a[j[o[length(j)]]]), c(b[j[o[length(j)]]], b[j[o[length(j)]]]), 
      col = 4, lwd = 2, lty = 2)
points(a[j], b[j], pch = 20, cex = 1.7, col = 4)

# Right panel: Pareto front on plateau function
plot(ent, s.dgp, pch = 20, col = "grey", cex = 0.5, cex.main = 1.8,
     xlab = "Entropy", ylab = "Predictive Standard Deviation",
     main = "Pareto front on plateau example")
points(ent[inds], s.dgp[inds], col = "purple", pch = 17, cex = 1.7)
points(ent.cand[inds.cand], s.dgp.cand[inds.cand], col = "green", 
       pch = 18, cex = 2.5)
points(ent.cand, s.dgp.cand, pch = 1, col = 1, cex = 1.2)

dev.off()

# Appendix: GP in two dimensions ----------------------------------------------

# Use previous one layer fit, predict in 2d
mean.gp <- rep(NA, times = np)
s.gp <- rep(NA, times = np)
for (i in 1:length(chunks)) {
  p <- predict(fit.gp, xgrid[chunks[[i]], ], lite = TRUE)
  mean.gp[chunks[[i]]] <- p$mean
  s.gp[chunks[[i]]] <- sqrt(p$s2)
}

# Calculate post hoc entropy and pareto front
failprob <- pnorm((mean.gp - limit) / s.gp)
ent <- -(1 - failprob) * log(1 - failprob) - failprob * log(failprob)
ent[which(is.nan(ent))] <- 0

# pdf("~/aero-git/papers/figures/plateau_gp.pdf", width = 11, height = 4)
par(mfrow = c(1, 3), mar = c(5, 5, 4, 2))

# Left panel: DGP predicted mean
image(g, g, matrix(mean.gp, nrow = ng), col = heat.colors(128),
      xlab = expression("x"[1]), ylab = expression("x"[2]), 
      main = "GP Predicted Mean", cex.main = 1.8, cex.lab = 1.5)
points(x[, 1], x[, 2])
contour(g, g, matrix(ygrid, nrow = ng), add = TRUE, level = limit,
        col = 4, lty = 1, lwd = 2)
contour(g, g, matrix(mean.gp, nrow = ng), add = TRUE, level = limit,
        col = 1, lty = 2, lwd = 2)
box()

# Middle panel: DGP predicted standard deviation
image(g, g, matrix(s.gp, nrow = ng), col = heat.colors(128),
      xlab = expression("x"[1]), ylab = expression("x"[2]), cex.main = 1.8,
      main = "GP Predicted Std. Dev.", cex.lab = 1.5)
points(x[, 1], x[, 2])
contour(g, g, matrix(ygrid, nrow = ng), add = TRUE, level = limit,
        col = 4, lty = 1, lwd = 2)
contour(g, g, matrix(mean.gp, nrow = ng), add = TRUE, level = limit,
        col = 1, lty = 2, lwd = 2)
box()

# Right panel: DGP predicted entropy
image(g, g, matrix(ent, nrow = ng), col = heat.colors(128),
      cex.main = 1.8,
      xlab = expression("x"[1]), ylab = expression("x"[2]), 
      main = "GP Predicted Entropy", cex.lab = 1.5)
points(x[, 1], x[, 2])
contour(g, g, matrix(ygrid, nrow = ng), add = TRUE, level = limit,
        col = 4, lty = 1, lwd = 2)
contour(g, g, matrix(mean.gp, nrow = ng), add = TRUE, level = limit,
        col = 1, lty = 2, lwd = 2)
box()

# dev.off()

# Figure ?: computation times -------------------------------------------------

col <- RColorBrewer::brewer.pal(6, "Set2")[c(1, 2, 3, 4)]

n0_list <- list(plateau2 = 5, plateau5 = 20, tray2 = 50)
N_list <- list(plateau2 = 30, plateau5 = 150, tray2 = 300)
ylims <- list(plateau2 = c(-0.01, 9), plateau5 = c(-0.5, 65), tray2 = c(-1, 120))
titles <- list(plateau2 = "2d plateau", plateau5 = "5d plateau", tray2 = "2d cross-in-tray")

# pdf(file = "~/aero-git/papers/figures/time.pdf", width = 13, height = 4)
par(mfrow = c(1, 3), mar = c(4, 5, 4, 1))
for (sim in c("plateau2", "plateau5", "tray2")) {
  n0 <- n0_list[[sim]]
  N <- N_list[[sim]]
  ylim <- ylims[[sim]]
  title <- titles[[sim]]
  
  dgp <- read.csv(paste0(sim, "/compute_time/dgp_pareto_seed1.csv"))
  if (sim == "plateau2") dgp3 <- read.csv(paste0(sim, "/compute_time/dgp3_pareto_seed1.csv"))
  gpMCMC <- read.csv(paste0(sim, "/compute_time/gpMCMC_pareto_seed1.csv"))
  gpMLE <- read.csv(paste0(sim, "/compute_time/gpMLE_seed1.csv"))

  plot((n0 + 1):N, dgp$model_time, type = "l", col = col[1], lwd = 2.5, xlab = "N",
       ylab = "Seconds", ylim = ylim, main = title, cex.axis = 1.2, 
       cex.lab = 1.2, cex.main = 1.6)
  lines((n0 + 1):N, dgp$acq_time, col = col[1], lwd = 2.5, lty = 2)
  if (sim == "plateau2") {
    lines((n0 + 1):N, dgp3$model_time, col = col[4], lwd = 2.5)
    lines((n0 + 1):N, dgp3$acq_time, col = col[4], lwd = 2.5, lty = 2)
  }
  lines((n0 + 1):N, gpMCMC$model_time, col = col[2], lwd = 2.5)
  lines((n0 + 1):N, gpMCMC$acq_time, col = col[2], lwd = 2.5, lty = 2)
  lines((n0 + 1):N, gpMLE$model_time, col = col[3], lwd = 2.5)
  lines((n0 + 1):N, gpMLE$acq_time, col = col[3], lwd = 2.5, lty = 2)
  if (sim == "plateau2") 
    legend("topright", c("DGP ESS - model update", "DGP ESS - pareto acquisition",
                         "DGP3 ESS - model update", "DGP3 ESS - pareto acquisition",
                      "GP MCMC - model update", "GP MCMC - pareto acquisition",
                      "GP MLE - model update", "GP MLE - hyb ent acquisition"),
         lty = c(1, 2, 1, 2, 1, 2, 1, 2), col = rep(col[c(1, 4, 2, 3)], each = 2), lwd = 2.5, cex = 1.3)
}
# dev.off()



