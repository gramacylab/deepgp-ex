
# Two-dimensional Plateau Example

* **alpha**: contains code for conducting multi-fidelity importance sampling to estimate failure probabilities
* **designs**: contains sequential designs (response in final column)
* **results**: contains calculated sensitivities for the sequential designs
* **static_fits**: contains R/python scripts used to fit surrogates to static LHS designs
* **dgpESS.R**: conducts sequential design for ESS DGP, using pareto tricands acquisitions
* **gpMCMC.R**: conducts sequential design for the MCMC GP, using pareto tricands acquisitions
* **gpMLE.py**: conducts sequential design for the MLE GP, using hybrid entropy acquisitions
* **plot.R**: reads results and generates plots
* **run.sh**: bash script to run in parallel