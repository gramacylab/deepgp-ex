
import numpy as np
import pandas as pd
import sys
from pyDOE import lhs
from scipy.stats import norm
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import Matern

import torch
from torch.utils.data import TensorDataset, DataLoader
sys.path.append("../../")
from dgpPYTORCH import DeepGP, train_deepGP
from botorch.utils.transforms import standardize

#seed = 1
seed = int(sys.argv[1])
np.random.seed(seed)

# Define function and settings ------------------------------------------------

def f(x):
    x = x * 4 - 2
    xs = -4 - 3 * np.sum(x, 1)
    y = 2 * norm.cdf(xs * np.sqrt(2)) - 1
    return y
    
limit = 0                    # response value for failure contour
d = 2                        # input dimension
N = 30                       # size of final design (n0 + acquisitions)
npred = 1000                 # size of LHS for testing
g = 1e-6                     # fixed nugget/noise 

x = lhs(d, N)
y = f(x)
xp = lhs(d, npred)
yp = f(xp)

fail = (yp > limit)
xp = xp[fail, :]
yp = yp[fail]

# GP MLE ----------------------------------------------------------------------

kernel = 1 * Matern(length_scale = [0.1] * d, length_scale_bounds = (0.001, 10), 
                    nu = 2.5)
gp = GPR(kernel, alpha = g)
gp.fit(x, y)
mu = gp.predict(xp)
sens = np.mean(mu > limit)

results = pd.read_csv("static_sens.csv")
results.loc[len(results.index)] = ["gpMLE", seed, N, sens]
results.to_csv("static_sens.csv", index = False)

# DGP PYTORCH -----------------------------------------------------------------

x   = torch.tensor(x, dtype = torch.float32)
y   = torch.tensor(y, dtype = torch.float32)
xp  = torch.tensor(xp , dtype = torch.float32)
yp  = torch.tensor(yp , dtype = torch.float32)

batch_size  = 30
num_epochs  = 2500
num_samples = 32
learning_rate = 1e-3

model = DeepGP(x.shape)
model = train_deepGP(model, x, standardize(y), num_epochs = num_epochs, 
                     batch_size = batch_size, learning_rate = learning_rate, 
                     num_samples = num_samples)
model.metrics(model, xp, yp, y, batch_size = batch_size)
mu = model.mu_pred.detach().numpy()
sens = np.mean(mu > limit)

results = pd.read_csv("static_sens.csv")
results.loc[len(results.index)] = ["dgpPYTORCH", seed, N, sens]
results.to_csv("static_sens.csv", index = False)
        
