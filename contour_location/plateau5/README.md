
# Five-dimensional Plateau Example

* **alpha**: contains code for conducting multi-fidelity importance sampling to estimate failure probabilities
* **results**: contains calculated sensitivities for the sequential designs
* **dgpESS.R**: conducts sequential design for ESS DGP, using pareto tricands acquisitions
* **gpMCMC.R**: conducts sequential design for the MCMC GP, using pareto tricands acquisitions
* **gpMLE.py**: conducts sequential design for the MLE GP, using hybrid entropy acquisitions
* **plot.R**: reads results and generates plots
* **run_python.sh**: bash script to run python script in parallel
* **run_R.sh**: bash script to run R in parallel
* **static_sens.csv**: stores sensitivities for static fits
* **static.py**: fits dgpPYTORCH and gpMLE models to static LHS's
* **static.R**: fits dgpESS and gpMCMC to static LHS's