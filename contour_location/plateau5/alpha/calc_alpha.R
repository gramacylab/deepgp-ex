
library(deepgp)
library(mclust)
library(mvtnorm)
library(laGP) # for MLE GP in R
source("../../gmm_functions.R")

xdens <- function(x) {
  # Returns input density
  dens <- 1
  for (i in 1:ncol(x))
    dens <- dens * dnorm(x[, i], 0.5, 0.1)
  return(dens)
}

xsample <- function(n, d = 5) {
  # Returns samples from the input distribution
  return(matrix(rnorm(n * d, 0.5, 0.1), nrow = n, ncol = d))
}

# Set up ----------------------------------------------------------------------

f <- function(x) {
  x <- x * 4 - 2
  xs <- -4 - 3*apply(x, 1, sum)
  y <- 2 * pnorm(xs * sqrt(2)) - 1
  return(y)
}
limit <- 0

method <- 1
seed <- 1
args <- commandArgs(TRUE)
if(length(args) > 0) 
  for(i in 1:length(args)) 
    eval(parse(text = args[[i]]))
cat("seed is ", seed, "\n")
set.seed(1) # random seed does not determine starting design anymore
if (method == 1) {
  method <- "gpMLE_hybrid"
} else if (method == 2) {
  method <- "gpMCMC_pareto"
} else method <- "dgp_pareto"
cat("method is", method, "\n")

# Estimate failure probability ------------------------------------------------

# Read design
design <- read.csv(paste0("../designs/", method, "_seed", seed, ".csv"))
x <- as.matrix(design[, 1:5])
y <- design[, 6]

# Fit surrogate
if (method == "gpMLE_hybrid") {
  gp <- newGPsep(x, y, d = rep(1, times = 5), g = 1e-6, dK = TRUE)
  mle <- mleGPsep(gp, param = "d", tmin = 0.01, tmax = 10)
} else if (method == "gpMCMC_pareto") {
  fit <- fit_one_layer(x, y, nmcmc = 10000, sep = TRUE, true_g = 1e-6)
  fit <- trim(fit, 5000, 5)
} else {
  fit <- fit_two_layer(x, y, nmcmc = 10000, true_g = 1e-6)
  fit <- trim(fit, 5000, 5)
} 

# Sample from input distribution and use surrogate to identify failures
xs <- xsample(10000) 
if (method == "gpMLE_hybrid") {
  pred <- predGPsep(gp, xs, lite = TRUE)
  deleteGPseps()
} else pred <- predict(fit, xs, lite = TRUE, cores = 8)
pred_failed <- xs[pred$mean > limit, ]

# Fit bias distribution
bias <- Mclust(pred_failed, verbose = FALSE)

# Generate samples from the bias distribution, evaluate true function
xbias <- mclust_sample(100, bias)
ybias <- f(xbias)

# Calculate alpha
weights <- xdens(xbias) / mclust_dens(xbias, bias)
alpha <- mean((ybias > limit) * weights)

alpha_df <- read.csv("alpha.csv")
alpha_df <- rbind(alpha_df, c(method, seed, alpha))
write.csv(alpha_df, "alpha.csv", row.names = FALSE)
