#!/bin/bash

method=3

for (( seed=1; seed<=30; seed++ ))
do
  R CMD BATCH "--args seed=$seed method=$method" calc_alpha.R &
  sleep 2
done
