
# Two-dimensional Cross-in-Tray Example

* **alpha**: contains code for conducting multi-fidelity importance sampling to estimate failure probabilities
* **results**: contains calculated sensitivities for the sequential designs
* **design_dgp_seed1.csv**: one resulting design for the dgpESS model with pareto tricands acquisitions
* **design_gpMLE_seed1.csv**: one resulting design for the gpMLE model with hybrid entropy acquisitions
* **dgpESS_continue.R**: restarts a started sequential design for the ESS DGP model
* **dgpESS.R**: conducts sequential design for ESS DGP, using pareto tricands acquisitions
* **gpMCMC.R**: conducts sequential design for the MCMC GP, using pareto tricands acquisitions
* **gpMLE.py**: conducts sequential design for the MLE GP, using hybrid entropy acquisitions
* **plot.R**: reads results and generates plots
* **run_python.sh**: bash script to run python script in parallel
* **run_R.sh**: bash script to run R in parallel
* **static_sens.csv**: stores sensitivities for static fits
* **static.py**: fits dgpPYTORCH and gpMLE models to static LHS's
* **static.R**: fits dgpESS and gpMCMC to static LHS's