
#!/bin/bash

method=2

for (( seed=1; seed<=4; seed++ ))
do
  R CMD BATCH "--args seed=$seed method=$method" calc_alpha.R &
  sleep 2
done
