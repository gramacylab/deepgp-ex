
library(deepgp)
library(lhs)
library(geometry)
source("../tricands.R")

# Read command line arguments -------------------------------------------------

seed <- 1
args <- commandArgs(TRUE)
if(length(args) > 0) 
  for(i in 1:length(args)) 
    eval(parse(text = args[[i]]))
cat("seed is ", seed, "\n")
set.seed(seed)

# Define function and settings ------------------------------------------------

f <- function(x) {
  x <- x * 4 - 2
  p1 <- abs(100 - sqrt(apply(x^2, 1, sum)) / pi)
  p2 <- abs(apply(sin(x), 1, prod) * exp(p1)) + 1
  y <- -0.0001 * (p2)^(0.1)
  return((y + 1.9) / 0.2)
}
limit <- 2
d <- 2
N <- 300
np <- 5000
g <- 1e-6

load(paste0("RData/dgp_seed", seed, ".RData")) # i, xp, fit, sens
x <- fit$x
y <- fit$y
last_run <- i
sens <- c(sens, rep(NA, 100))

# Conduct sequential design ---------------------------------------------------

for (i in (last_run + 1):(N - n0)) {
    
  cat(i, "\n")
  fit <- predict(fit, xp, lite = TRUE, cores = 1)
  sens[i] <- mean(fit$mean > limit)
  
  if (i == (N - n0)) break
  
  o <- order(abs(y - limit), decreasing = FALSE)
  xcand <- tricands(x, p = 0.9, max = 100 * d, ordering = o)
  # cat("Number of tricands = ", nrow(xcand), "\n")
  pcand <- predict(fit, xcand, lite = TRUE, cores = 1)
  s <- sqrt(pcand$s2)
  fail_prob <- pnorm((pcand$mean - limit) / s)
  ent <- -(1 - fail_prob) * log(1 - fail_prob) - fail_prob * log(fail_prob)
  ent[which(is.nan(ent))] <- 0
  inds <- get_pareto(ent, s)
  if (length(inds) == 1) ru <- inds else ru <- sample(inds, 1)
  new_x <- matrix(xcand[ru, ], nrow = 1)
  
  x <- rbind(x, new_x)
  new_y <- f(new_x)
  y <- c(y, new_y)

  fit <- fit_two_layer(x, y, nmcmc = 1000, true_g = g,
                       w_0 = fit$w[[fit$nmcmc]],
                       theta_y_0 = fit$theta_y[fit$nmcmc],
                       theta_w_0 = fit$theta_w[fit$nmcmc, ])
  fit <- trim(fit, 500)
  save(i, xp, fit, sens, file = paste0("RData/dgp_seed", seed, ".RData"))
}

write.csv(cbind(x, y), paste0("designs/dgp_pareto_seed", seed, ".csv"), 
          row.names = FALSE, header = FALSE)
write.csv(sens, paste0("results/dgp_pareto_seed", seed, ".csv"), 
          row.names = FALSE)

