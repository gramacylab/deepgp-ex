
import numpy as np
import sys
from pyDOE import lhs
from scipy.stats import norm
from scipy.optimize import minimize
import warnings
warnings.filterwarnings(action = "ignore", category = RuntimeWarning)
    # for entropy calculation on really small or large probabilities
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import Matern
import time

def neg_entropy(x, gp, limit):
    if len(x.shape) == 1: 
        x = x.reshape(1, d)
    mu, std = gp.predict(x, return_std = True)
    fail_prob = norm.cdf((mu - limit) / std)
    ent = -(1 - fail_prob) * np.log(1 - fail_prob) - fail_prob * np.log(fail_prob)
    ent[np.isnan(ent)] = 0
    return -ent

# Set seed for Monte Carlo exercise -------------------------------------------

#seed = 1
seed = int(sys.argv[1])
np.random.seed(seed)

# Define function and settings ------------------------------------------------

def f(x):
    x = x * 4 - 2
    p1 = abs(100 - np.sqrt(np.sum(x**2, 1)) / np.pi)
    p2 = abs(np.prod(np.sin(x), 1) * np.exp(p1)) + 1
    y = -0.0001 * (p2)**(0.1)
    return ((y + 1.9) / 0.2)
    
limit = 2                    # response value for failure contour
d = 2                        # input dimension
n0 = 50                      # size of starting LHS design
N = 300                      # size of final design (n0 + acquisitions)
npred = 5000                 # size of LHS for testing
g = 1e-6                     # fixed nugget/noise 

sens = np.zeros(N - n0)
model_time = np.zeros(N - n0)
acq_time = np.zeros(N - n0)

# Conduct sequential design ---------------------------------------------------

x = lhs(d, n0)
y = f(x)
xp = lhs(d, npred)
yp = f(xp)

# Since we are only calculating sensitivity, we only need to predict at failures
fail = (yp > limit)
xp = xp[fail, :]
yp = yp[fail]

# Fit initial model   
tic = time.time()
kernel = 1 * Matern(length_scale = [0.1] * d, 
                    length_scale_bounds = (0.001, 10), nu = 2.5)
gp = GPR(kernel, alpha = g)
gp.fit(x, y)
toc = time.time()
model_time[0] = toc - tic

for i in range(N - n0):
    
    # Evaluate sensitivity and store result
    mu = gp.predict(xp)
    sens[i] = np.mean(mu > limit)
    
    if i == (N - n0 - 1): break

    # Hybrid entropy optimization
    tic = time.time()
    xbatch = lhs(d, 10 * d)
    best_batch = np.argmin(neg_entropy(xbatch, gp, limit))
    xinit = xbatch[best_batch, :]
    opt = minimize(neg_entropy, xinit, method='L-BFGS-B', 
                   bounds = ((0, 1), (0, 1)),
                   args=(gp, limit))
    new_x = opt['x'].reshape(1, d)
    toc = time.time()
    acq_time[i] = toc - tic

    # Augment data with new acquisitions
    x = np.row_stack((x, new_x))
    new_y = f(new_x)
    y = np.append(y, new_y)
    
    # Update surrogate with new data point
    tic = time.time()
    gp.fit(x, y)
    toc = time.time()
    model_time[i + 1] = toc - tic

np.savetxt("designs/gpMLE_hybrid_seed" + str(seed) + ".csv", 
           np.column_stack((x, y)), delimiter = ",", header = "x1,x2,y")
np.savetxt("results/gpMLE_hybrid_seed" + str(seed) + ".csv", sens,
           header = 'x')
np.savetxt("compute_time/gpMLE_seed" + str(seed) + ".csv",
           np.column_stack((model_time, acq_time)), delimiter = ",",
           header = "model_name,acq_name")


