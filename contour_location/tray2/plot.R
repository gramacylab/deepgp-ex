
library(latex2exp)

n0 <- 50
N <- 300
reps <- 20
methods <- c("dgp_pareto", "gpMCMC_pareto", "gpMLE_hybrid")
labels <- c("DGP ESS pareto", "GP MCMC pareto", "GP MLE hyb ent")

results <- list()
for (j in 1:length(methods)) {
  results[[j]] <- matrix(NA, nrow = N - n0, ncol = reps)
  for (i in 1:reps) {
    file_name <- paste0("results/", methods[j], "_seed", i, ".csv")
    if (file.exists(file_name))
      results[[j]][, i] <- read.csv(file_name)[[1]]
  }
}

final <- data.frame(a = unlist(results[[1]][N - n0, ]))
for (i in 2:length(methods))
  final <- cbind(final, unlist(results[[i]][N - n0, ]))

static <- read.csv("static_fits/static_sens.csv")
final <- cbind(final, c(static$SENS[which(static$MODEL == "dgp")]))
final <- cbind(final, c(static$SENS[which(static$MODEL == "gpMCMC")]))
final <- cbind(final, c(static$SENS[which(static$MODEL == "gpMLE")]))
final <- cbind(final, c(static$SENS[which(static$MODEL == "dgpPYTORCH")]))

col <- RColorBrewer::brewer.pal(6, "Set2")[c(1, 2, 3, 6)]

# pdf(file = "~/aero-git/papers/figures/tray_results.pdf", width = 13, height = 6)
par(mfrow = c(1, 2), mar = c(7, 5, 4, 1))
plot(0, 0, pch = NA, xlim = c(n0, N), ylim = c(0.15, 1),
     xlab = "N", ylab = TeX(r"(Sensitivity\;\rightarrow$)"), 
     main = "Median Sensitivity (20 reps)",
     las = 1, cex.axis = 1.2, cex.lab = 1.2, cex.main = 1.4)
lines((n0 + 1):N, apply(results[[1]], 1, median, na.rm = T), col = col[1], lwd = 4, lty = 1)
lines((n0 + 1):N, apply(results[[2]], 1, median, na.rm = T), col = col[2], lwd = 4, lty = 2)
lines((n0 + 1):N, apply(results[[3]], 1, median, na.rm = T), col = col[3], lwd = 4, lty = 4)
legend("bottomright", legend = labels, col = col, lty = c(1, 2, 4), ncol = 1, lwd = 3)
boxplot(final, col = c(col[1:3], col[1:3], col[4]), las = 2, names = NA, 
        cex.main = 1.4,
        ylim = c(0.15, 1), main = "Final Sensitivity at N = 300",
        cex.axis = 1.2, cex.lab = 1.2)
text(1:7 - 0.6, par("usr")[3] - 0.13, 
     labels = c(labels, "DGP ESS LHS", "GP MCMC LHS", "GP MLE LHS", "DGP VI LHS"),
     srt = 45, pos = 1, xpd = TRUE, cex = 1.2)
abline(v = 3.5, col = "darkgrey", lty = 2, lwd = 3)
text(2, 0.22, "sequential \n designs", cex = 1.5)
text(5, 0.22, "static LHS", cex = 1.5)
# dev.off()

f <- function(x) {
  x <- x * 4 - 2
  p1 <- abs(100 - sqrt(apply(x^2, 1, sum)) / pi)
  p2 <- abs(apply(sin(x), 1, prod) * exp(p1)) + 1
  y <- -0.0001 * (p2)^(0.1)
  return((y + 1.9) / 0.2)
}
limit <- 2

grid <- seq(0, 1, length = 100)
x <- as.matrix(expand.grid(grid, grid))
y <- f(x)

x1 <- read.csv("designs/dgp_pareto_seed2.csv")
x2 <- read.csv("designs/gpMLE_hybrid_seed2.csv")

# pdf(file = "~/aero-git/papers/figures/tray_design.pdf", width = 11, height = 4)
par(mfrow = c(1, 3), mar = c(5, 5, 4, 2))
image(grid, grid, matrix(y, ncol = 100), col = heat.colors(128),
      xlab = expression("x"[1]), ylab = expression("x"[2]),
      main = "Cross-in-Tray function",
      cex.main = 1.8, cex.axis = 1.2, cex.lab = 1.5)
contour(grid, grid, matrix(y, ncol = 100), level = limit, 
        col = 4, lwd = 3, add = TRUE)
box(lwd = 1)
plot(x1[, 1:2], col = c(rep(2, 50), rep(1, 250)), pch = c(rep(4, 50), rep(1, 250)),
     main = "DGP ESS pareto design", xlab = expression("x"[1]), 
     ylab = expression("x"[2]),
     cex.main = 1.8, cex.axis = 1.2, cex.lab = 1.5, cex = 1.2)
plot(x2[, 1:2], col = c(rep(2, 50), rep(1, 250)), pch = c(rep(4, 50), rep(1, 250)),
     main = "GP MLE hyb ent design", xlab = expression("x"[1]), 
     ylab = expression("x"[2]),
     cex.main = 1.8, cex.axis = 1.2, cex.lab = 1.5, cex = 1.2)
# dev.off()

# For AIAA Scitech - add in failure probability estimates
alpha <- read.csv("alpha/alpha.csv")
true_alpha <- 0.048352

# pdf(file = "~/aero-git/papers/scitech23/figures/tray_scitech.pdf", width = 13, height = 4.5)
layout(matrix(1:3, nrow = 1), widths = c(0.38, 0.38, 0.24))
par(mar = c(7, 5, 4, 1))
plot(0, 0, pch = NA, xlim = c(n0, N), ylim = c(0.15, 1),
     xlab = "N", ylab = TeX(r"(Sensitivity\;\rightarrow$)"), 
     main = "Median Sensitivity (20 reps)",
     las = 1, cex.axis = 1.2, cex.lab = 1.2, cex.main = 1.6)
lines((n0 + 1):N, apply(results[[1]], 1, median, na.rm = T), col = col[1], lwd = 4, lty = 1)
lines((n0 + 1):N, apply(results[[2]], 1, median, na.rm = T), col = col[2], lwd = 4, lty = 2)
lines((n0 + 1):N, apply(results[[3]], 1, median, na.rm = T), col = col[3], lwd = 4, lty = 4)
legend("bottomright", legend = labels, col = col, lty = c(1, 2, 4), ncol = 1, lwd = 3,
       cex = 1.5)
boxplot(final, col = c(col[1:3], col), las = 2, names = NA, 
        cex.main = 1.6,
        ylim = c(0.15, 1), main = "Final Sensitivity at N = 300",
        cex.axis = 1.2, cex.lab = 1.2)
text(1:7 - 0.6, par("usr")[3] - 0.11, 
     labels = c(labels, "DGP ESS LHS", "GP MCMC LHS", "GP MLE LHS", "DGP VI LHS"),
     srt = 45, pos = 1, xpd = TRUE, cex = 1.2)
abline(v = 3.5, col = "darkgrey", lty = 2, lwd = 3)
text(2, 0.22, "sequential \n designs", cex = 1.5)
text(5, 0.22, "static LHS", cex = 1.5)
boxplot(alpha$alpha ~ alpha$method, col = col[1:3], las = 2, names = NA,
        cex.main = 1.6, main = "Failure Probability",
        ylab = "", xlab = "", cex.axis = 1.2, cex.lab = 1.2)
abline(h = true_alpha, lty = 5, lwd = 4, col = 2)
text(1:3 - 0.45 , par("usr")[3] - 0.011, labels = labels,
     srt = 45, pos = 1, xpd = TRUE, cex = 1.2)
# dev.off()

