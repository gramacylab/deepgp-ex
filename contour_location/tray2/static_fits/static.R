
library(deepgp)
library(lhs)

seed <- 1
args <- commandArgs(TRUE)
if(length(args) > 0) 
  for(i in 1:length(args)) 
    eval(parse(text = args[[i]]))
cat("seed is ", seed, "\n")
set.seed(seed)

# Define function and settings ------------------------------------------------

f <- function(x) {
  x <- x * 4 - 2
  p1 <- abs(100 - sqrt(apply(x^2, 1, sum)) / pi)
  p2 <- abs(apply(sin(x), 1, prod) * exp(p1)) + 1
  y <- -0.0001 * (p2)^(0.1)
  return((y + 1.9) / 0.2)
}
limit <- 2
d <- 2
N <- 300
np <- 5000
g <- 1e-6

x <- randomLHS(N, d)
y <- f(x)
xp <- randomLHS(np, d)
yp <- f(xp)

fail <- which(yp > limit)
xp <- xp[fail, ]
yp <- yp[fail]

# GP MCMC ---------------------------------------------------------------------

fit <- fit_one_layer(x, y, nmcmc = 10000, sep = TRUE, true_g = g)
fit <- trim(fit, 5000, 5)
fit <- predict(fit, xp, lite = TRUE, cores = 1)
sens <- mean(fit$mean > limit)

results <- read.csv("static_sens.csv")
results <- rbind(results, c("gpMCMC", seed, N, sens))
write.csv(results, "static_sens.csv", row.names = FALSE)

# DGP -------------------------------------------------------------------------

fit <- fit_two_layer(x, y, nmcmc = 10000, true_g = g)
fit <- trim(fit, 5000, 5)
fit <- predict(fit, xp, lite = TRUE, cores = 1)
sens <- mean(fit$mean > limit)

results <- read.csv("static_sens.csv")
results <- rbind(results, c("dgp", seed, N, sens))
write.csv(results, "static_sens.csv", row.names = FALSE)
