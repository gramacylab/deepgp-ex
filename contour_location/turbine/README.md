
# Two-dimensional Turbine Simulator

* **alpha**: contains R scripts and data for conducting multi-fidelity importance sampling to estimate the failure probability
* **data**: contains two LHS designs as well as row indices for train/test splits
* **run_simulator**: contains Matlab code and python wrapper for running the turbine simulator
* **seq_design**: contains R/python scripts used to conduct sequential designs for each surrogate (as well as results and the resulting designs)
* **static**: contains R/python scripts used to fit surrogates to static LHS designs
* **plot.R**: R code used to generate figures
* **scaling_functions.R**: R code used to scale inputs