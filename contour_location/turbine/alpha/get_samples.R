
# For each model (gpMLE, gpMCMC, and dgpESS), load the bias distribution and 
# generate 100 samples from it (this will be evaluated through the simulator)

library(deepgp)
library(mclust)
library(mvtnorm)
library(laGP)
source("../scaling_functions.R")
source("../../gmm_functions.R")

seed <- 1
set.seed(seed)

for (method in c("dgpESS", "gpMCMC", "gpMLE")) {

  load(paste0("bias/fit_", method, "_seed", seed, ".RData"))
  xbias <- mclust_sample(100, bias)
  xbias <- scale_up(xbias)
  write.csv(xbias, paste0("bias/samples_", method, "_seed", seed, ".csv"), 
            row.names = FALSE)
}
