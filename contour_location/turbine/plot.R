
library(latex2exp)
source("scaling_functions.R")

# Visualize surface

dat <- read.csv("data/lhs1000.csv")
x <- as.matrix(dat[, 1:2])
x <- scale_down(x)
y <- dat[, 3]

reps <- 20
n0 <- 5
N <- 30
methods <- c("dgpESS", "gpMCMC", "gpMLE")
metric <- "SENS"

results <- list()
for (j in 1:length(methods)) {
  results[[j]] <- matrix(NA, nrow = N - n0 + 1, ncol = reps)
  for (i in 1:reps) {
    file_name <- paste0("seq_design/results/", methods[j], "_seed", i, ".csv")
    results[[j]][, i] <- read.csv(file_name)[[metric]]
  }
}

final <- data.frame(a = unlist(results[[1]][N - n0 + 1, ]))
for (i in 2:length(methods))
  final <- cbind(final, unlist(results[[i]][N - n0 + 1, ]))

static <- read.csv("static/static_results.csv")
final <- cbind(final, static[[metric]][which(static$MODEL == "dgpESS")][1:reps])
final <- cbind(final, static[[metric]][which(static$MODEL == "gpMCMC")][1:reps])
final <- cbind(final, static[[metric]][which(static$MODEL == "gpMLE")][1:reps])

col <- RColorBrewer::brewer.pal(6, "Set2")[c(1, 2, 3, 6)]
methods <- c("DGP ESS pareto", "GP MCMC pareto", "GP MLE hyb ent")

pdf(file = "~/aero-git/papers/scitech23/figures/turbine_scitech.pdf", width = 13, height = 4.5)

par(mfrow = c(1, 3), mar = c(7, 5, 4, 1))

i <- interp::interp(x[, 1], x[, 2], y)
image(i, col = heat.colors(128), xlab = "X1", ylab = "X2", 
      main = "Turbine Surface w/ Contour", cex.main = 1.6)
contour(i, add = TRUE, level = limit, labels = "", col = 4, lwd = 3)

ymin <- min(c(results[[1]], results[[2]], results[[3]]))
ymax <- max(c(results[[1]], results[[2]], results[[3]]))
plot(0, 0, pch = NA, xlim = c(n0, N + 1), ylim = c(ymin, ymax),
     xlab = "N", ylab = TeX(r"(Sensitivity\;\rightarrow$)"), 
     main = "Median Sensitivity (20 reps)",
     las = 1, cex.axis = 1.2, cex.lab = 1.2, cex.main = 1.6)
lines(n0:N, apply(results[[1]], 1, median, na.rm = T), col = col[1], lwd = 4, lty = 1)
lines(n0:N, apply(results[[2]], 1, median, na.rm = T), col = col[2], lwd = 4, lty = 2)
lines(n0:N, apply(results[[3]], 1, median, na.rm = T), col = col[3], lwd = 4, lty = 4)
legend("bottomright", legend = methods, col = col, lty = c(1, 2, 4), ncol = 1, lwd = 3,
       cex = 1.5)
boxplot(final, col = c(col[1:3], col), las = 2, names = NA, 
        cex.main = 1.6,
        ylim = c(ymin, ymax), main = "Final Sensitivity at N = 30",
        cex.axis = 1.2, cex.lab = 1.2)
text(1:6 - 0.6, par("usr")[3] - 0.13, 
     labels = c(methods, "DGP ESS LHS", "GP MCMC LHS", "GP MLE LHS"),
     srt = 45, pos = 1, xpd = TRUE, cex = 1.2)
abline(v = 3.5, col = "darkgrey", lty = 2, lwd = 3)
text(2, 0.2, "sequential \n designs", cex = 1.5)
text(5, 0.2, "static LHS", cex = 1.5)

dev.off()
