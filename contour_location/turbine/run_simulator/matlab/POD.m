% POD
%%
Y = csvread('2parameter_Snapshots.csv');
X = csvread('2parameter_DOE.csv');
%%
Ytrain = Y(:, 1:100);
Ytest = Y(:,100:end);
Ytrain_c = Ytrain -  mean(Ytrain, 2);
%%
[u, d, v] = svd(Ytrain_c, 0);
%% plot first 4 POD modes
for i=1:4
    figure
    pdeplot3D(smodel,'ColorMapData',u(:,i))
end