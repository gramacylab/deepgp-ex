function y = mf_turbineLSA(p1, p2, s, iter)
    disp('running matlab function')
    disp(s==.5);
    %%
    smodel = createpde('structural','static-solid');
    importGeometry(smodel,'Blade.stl');
    if s == 1.0
        load msh.mat
    end
    if s==.5
        load msh2.mat
    end
    if s == 0.0
        load msh3.mat
    end

    %%
    % CONSTANTS
    E   = 227E9; % in Pa
    CTE = 12.7E-6; % in 1/K
    nu  = 0.27; 
    structuralProperties(smodel,'YoungsModulus',E, ...
                                'PoissonsRatio',nu, ...
                                'CTE',CTE);
    structuralBC(smodel,'Face',3,'Constraint','fixed');
    
    structuralBoundaryLoad(smodel,'Face',11,'Pressure',p1); % Pressure side
    structuralBoundaryLoad(smodel,'Face',10,'Pressure',p2); % Suction side 
    
    Rs = solve(smodel);
        
    y = Rs.VonMisesStress;
    
    newdir= num2str(iter);
    mkdir(newdir)
    filename = strcat(newdir, '/', 'y.mat'); 
    save(filename, 'y')
    pause(3)
end