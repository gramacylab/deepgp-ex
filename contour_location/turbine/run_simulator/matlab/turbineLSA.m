function y = turbineLSA(p1, p2, iter)
    disp('running matlab function')
    %%
    smodel = createpde('structural','static-solid');
    importGeometry(smodel,'Blade.stl');
    msh = generateMesh(smodel,'Hmax',0.01);
    %%
    % CONSTANTS
    E   = 227E9; % in Pa
    CTE = 12.7E-6; % in 1/K
    nu  = 0.27; 
    structuralProperties(smodel,'YoungsModulus',E, ...
                                'PoissonsRatio',nu, ...
                                'CTE',CTE);
    structuralBC(smodel,'Face',3,'Constraint','fixed');
    
    structuralBoundaryLoad(smodel,'Face',11,'Pressure',p1); % Pressure side
    structuralBoundaryLoad(smodel,'Face',10,'Pressure',p2); % Suction side 
    
    Rs = solve(smodel);
        
    y = Rs.VonMisesStress;
    
    newdir= num2str(iter);
    mkdir(newdir)
    filename = strcat(newdir, '/', 'y.mat'); 
    save(filename, 'y')
    pause(3)
end