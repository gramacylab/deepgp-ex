%%
smodel = createpde('structural','static-solid');

importGeometry(smodel,'Blade.stl');
% figure
% pdegplot(smodel,'FaceLabels','on','FaceAlpha',0.5)

msh = generateMesh(smodel,'Hmax',0.01);

E = 227E9; % in Pa
CTE = 12.7E-6; % in 1/K
nu = 0.27; 
p1 = 5e5; %5e5; %in Pa
p2 = 4.5e5; %in Pa
%%
lb = [2e5, 3e5];
ub = [7e5, 6e5];
ndes = 150;
normalized_design = lhsdesign(ndes, 2);
unnormalized_design = lb + normalized_design.* (ub-lb);
snap = zeros(21252, ndes);

for i = 1:ndes
    
    structuralProperties(smodel,'YoungsModulus',E, ...
                            'PoissonsRatio',nu, ...
                            'CTE',CTE);
                        
    structuralBC(smodel,'Face',3,'Constraint','fixed');

    p1 = unnormalized_design(i, 1);
    p2 = unnormalized_design(i, 2);

    structuralBoundaryLoad(smodel,'Face',11,'Pressure',p1); % Pressure side
    structuralBoundaryLoad(smodel,'Face',10,'Pressure',p2);  % Suction side 

    Rs = solve(smodel);
    
    snap(:,i) = Rs.VonMisesStress;
%     figure
%     pdeplot3D(smodel,'ColorMapData',Rs.VonMisesStress, ...
%                  'Deformation',Rs.Displacement, ...
%                  'DeformationScaleFactor',100)
%     view([116,25]);
end