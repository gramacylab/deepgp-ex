function viz(out_file)
%     clear;
%     clc;
    %%
    smodel = createpde('structural','static-solid');
    
    importGeometry(smodel,'Blade.stl');
    
    load msh.mat
    %%
    D = csvread('predictions.csv');
    filename=out_file;
%     D = csvread(input_file);
    figure
    pdeplot3D(smodel,'ColorMapData',D);
    caxis([1   126510000]);
    saveas(gcf, filename)
    close all
%     figure
%     pdeplot3D(smodel,'ColorMapData',D(:,2) )
%     caxis([1   126510000]);
%     figure
%     pdeplot3D(smodel,'ColorMapData',D(:,1) - D(:,2) )
end

