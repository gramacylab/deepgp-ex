
import sys
import numpy as np
import torch
import argparse
import subprocess
from scipy.io import loadmat
torch.manual_seed(2436)
#from pyDOE import lhs

seed = 1
method = "dgpESS"

parser = argparse.ArgumentParser()
parser.add_argument("-matlab_path", "--matlab_path", type = str, const = 1, nargs = "?",
                    # default="/Applications/MATLAB_R2021b.app/bin/matlab", # Ashwin's local path
                    #default="/usr/local/MATLAB/R2023a/bin/matlab", # Annie's office
                    default="/Applications/MatLab2023a/MATLAB_R2023a.app/bin/matlab", # Annie's mac
                    help="path to matlab executable on your machine")
args = parser.parse_args()

def turbine_model(candidates, iter=0, rank=0):
    if type(candidates) is torch.Tensor:
        candidates = candidates.numpy()
    n, d = candidates.shape
    y = torch.zeros([n])
    for i in range(n):
        x1, x2 = candidates[i,0], candidates[i,1]
        print(f"args for matlab call {x1}, {x2}")
        matlab = [args.matlab_path]
        options = ["-nosplash", " -wait", " -nodisplay", " -nodesktop", " -nojvm", " -r"]
        command = ["'turbineLSA(%s, %s, %s); exit;'" % (str(x1), str(x2), str(iter))]
        out = subprocess.Popen(matlab + options + command, cwd="matlab/", shell=False)
        out.wait(25)
        vm = loadmat("matlab/" + str(rank) + "/y" + ".mat")["y"].squeeze().max() # max von-Mises stress
        print(f"max Von Mises stress {vm}", flush=True)
        y[i] = torch.tensor([ vm ], dtype=torch.float32)
    return y

## Run 1000 point LHS
#bounds = [[201160, 698390], [301460, 598020]]
#X = lhs(2, 1000)
#for i in range(2):
#    X[:,i] = X[:,i] * (bounds[i][1] - bounds[i][0]) + bounds[i][0]
#y = turbine_model(X)
#y = y.reshape(-1, 1)
#dat = np.hstack((X, y))
#np.savetxt("../data/lhs1000.csv", dat, delimiter = ',', header = 'x1,x2,y')

## Run single input
#X = np.loadtxt("X.csv", delimiter=",", skiprows = 1).reshape(-1, 2)
#y = turbine_model(X) 
#np.savetxt("y.csv", y)
 
## Run bias samples
X = np.loadtxt("../alpha/bias/samples_" + method + "_seed" + str(seed) + ".csv",
               delimiter = ",", skiprows = 1) 
#X = np.loadtxt("Xunif.csv", delimiter = ",", skiprows = 1)
y = turbine_model(X)
y = y.reshape(-1, 1)
dat = np.hstack((X, y))
np.savetxt("../alpha/bias/samples_" + method + "_seed" + str(seed) + ".csv",
           dat, delimiter = ",", header = "x1,x2,y")    
#np.savetxt("Xunif.csv", dat, delimiter = ",", header = "x1,x2,y")    
