
bounds <- data.frame("X1" = c(201160, 698390), "X2" = c(301460, 598020))

limit <- 249280870

scale_down <- function(x) {
  # Converts inputs from original scale to unit cube
  for (i in 1:ncol(x)) 
    x[, i] <- (x[, i] - bounds[1, i]) / (bounds[2, i] - bounds[1, i])
  return(x)
}

scale_up <- function(x) {
  # Converts inputs from unit cube to the original scale
  for (i in 1:ncol(x)) 
    x[, i] <- x[, i] * (bounds[2, i] - bounds[1, i]) + bounds[1, i]
  return(x)
}