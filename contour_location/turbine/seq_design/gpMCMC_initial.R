
# Start with a random LHS of size 5 (from hold out set)
# Fit initial GP model
# Use tricands/pareto to select next location
# Write next location to "../run_simulator/X.csv" file

library(deepgp)
library(geometry)
source("../scaling_functions.R")
source("~/deepgp-ex/contour_location/tricands.R")

# Read command line arguments -------------------------------------------------

seed <- 1
args <- commandArgs(TRUE)
if(length(args) > 0) 
  for(i in 1:length(args)) 
    eval(parse(text = args[[i]]))
cat("seed is ", seed)
set.seed(seed)

# Read data, get train/test split ---------------------------------------------

# Training data
train_dat <- read.csv("../data/lhs500.csv")
train <- read.csv(paste0("../data/train_seed", seed, ".csv"))[[1]]
train <- train[1:5]
x <- as.matrix(train_dat[train, 1:2])
x <- scale_down(x)
y <- train_dat[train, 3]
y_mean <- mean(y)
y_sd <- sd(y)
y <- (y - y_mean) / y_sd

# Testing data
test_dat <- read.csv("../data/lhs1000.csv")
fail <- (test_dat[, 3] > limit)
xp <- as.matrix(test_dat[fail, 1:2])
xp <- scale_down(xp)
yp <- test_dat[fail, 3]

# Fit GP ----------------------------------------------------------------------

fit <- fit_one_layer(x, y, nmcmc = 5000, sep = TRUE, true_g = 1e-6)
fit <- trim(fit, 3000, 2)

# Predict at testing set
p <- predict(fit, xp, lite = TRUE)
p$mean <- (p$mean * y_sd) + y_mean 
sens <- mean(p$mean > limit)

results <- data.frame(matrix(NA, ncol = 2, nrow = 1))
colnames(results) <- c("N", "SENS")
results <- rbind(results, c(nrow(x), sens))
write.csv(results[-1, ], paste0("results/gpMCMC_seed", seed, ".csv"), 
          row.names = FALSE)

# Select next design point ----------------------------------------------------

# Scale limit down
limit <- (limit - y_mean) / y_sd

# Calculate tricands in the [0, 1]^d bounding box
o <- order(abs(y - limit), decreasing = FALSE)
xcand <- tricands(x, p = 0.9, ordering = o)

# Evaluate entropy and predictive standard deviation
pcand <- predict(fit, xcand, lite = TRUE)
s <- sqrt(pcand$s2)
fail_prob <- pnorm((pcand$mean - limit) / s)
ent <- -(1 - fail_prob) * log(1 - fail_prob) - fail_prob * log(fail_prob)
ent[which(is.nan(ent))] <- 0

# Choose point on pareto front
inds <- get_pareto(ent, s)
if (length(inds) == 1) ru <- inds else ru <- sample(inds, 1)
new_x <- matrix(xcand[ru, ], nrow = 1)

# Convert back to original scale, write to file
new_x <- scale_up(new_x)
colnames(new_x) <- colnames(x)
write.csv(new_x, "../run_simulator/X.csv", row.names = FALSE)

# Save input design to file (on original scale)
x <- scale_up(x)
y <- y * y_sd + y_mean
design <- cbind(x, y)
colnames(design) <- c("x1", "x2", "y")
write.csv(design, paste0("designs/gpMCMC_seed", seed, ".csv"), row.names = FALSE)

# Save fit
write.csv(fit$theta[fit$nmcmc, ], file = "last_sample.csv", row.names = FALSE)
