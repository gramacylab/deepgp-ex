
import numpy as np
import sys
from scipy.stats import norm
from scipy.optimize import minimize
import warnings
warnings.filterwarnings(action = "ignore", category = RuntimeWarning)
    # for entropy calculation on really small or large probabilities
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import Matern
import properscoring as ps
import pandas as pd
from pyDOE import lhs

bounds = [[201160, 698390], [301460, 598020]]
limit = 249280870

def scale_down(x):
    for i in range(x.shape[1]):
        x[:, i] = (x[:, i] - bounds[i][0]) / (bounds[i][1] - bounds[i][0])
    return x

def scale_up(x):
    for i in range(x.shape[1]):
        x[:, i] = x[:, i] * (bounds[i][1] - bounds[i][0]) + bounds[i][0]
    return x

def neg_entropy(x, gp, limit):
    if len(x.shape) == 1: 
        x = x.reshape(1, 2)
    mu, std = gp.predict(x, return_std = True)
    fail_prob = norm.cdf((mu - limit) / std)
    ent = -(1 - fail_prob) * np.log(1 - fail_prob) - fail_prob * np.log(fail_prob)
    ent[np.isnan(ent)] = 0
    return -ent

# Read command line arguments -------------------------------------------------

#seed = 1
seed = int(sys.argv[1])
np.random.seed(seed)

# Read data, get train/test split ---------------------------------------------

test_dat = np.loadtxt("../data/lhs1000.csv", delimiter = ",", skiprows = 1)
fail = (test_dat[:, 2] > limit)
xp = test_dat[fail, :][:, [0,1]]
xp = scale_down(xp)
yp = test_dat[fail, 2]

design = np.loadtxt("designs/gpMLE_seed" + str(seed) + ".csv", delimiter = ",", skiprows = 1)
new_x = np.loadtxt("../run_simulator/X.csv", delimiter = ",", skiprows = 1)
new_y = np.loadtxt("../run_simulator/y.csv").reshape(1)

x = np.vstack((design[:, [0, 1]], new_x))
x = scale_down(x)
y = np.hstack((design[:, 2], new_y))
y_mean = np.mean(y)
y_sd = np.std(y)
y = (y - y_mean) / y_sd

# Fit GP ----------------------------------------------------------------------

kernel = 1 * Matern(length_scale = [0.1, 0.1], length_scale_bounds = (0.001, 10), 
                    nu = 2.5)
gp = GPR(kernel, alpha = 1e-6)
gp.fit(x, y)

# Predict at testing set
mu = gp.predict(xp)
mu = (mu * y_sd) + y_mean
sens = np.mean(mu > limit)

results = pd.read_csv("results/gpMLE_seed" + str(seed) + ".csv")
results_new = pd.DataFrame(data = {"N": [x.shape[0]], "SENS": [sens]})
results = pd.concat((results, results_new))
results.to_csv("results/gpMLE_seed" + str(seed) + ".csv", index = False)

# Select next design point ----------------------------------------------------

# Scale limit down
limit = (limit - y_mean) / y_sd

# Hybrid entropy optimization
xbatch = lhs(2, 10 * 2)
best_batch = np.argmin(neg_entropy(xbatch, gp, limit))
xinit = xbatch[best_batch, :]
opt = minimize(neg_entropy, xinit, method='L-BFGS-B', 
               bounds = ((0, 1), (0, 1)), args = (gp, limit))
new_x = opt['x'].reshape(1, 2)

# Convert back to original scale, write to file
new_x = scale_up(new_x)
new_x = pd.DataFrame(new_x, columns = ["x1", "x2"])
new_x.to_csv("../run_simulator/X.csv", index = False)

# Save input design to file (on original scale)
x = scale_up(x)
y = y * y_sd + y_mean
design = np.hstack((x, y.reshape(-1, 1)))
design = pd.DataFrame(design, columns = ["x1", "x2", "y"])
design.to_csv("designs/gpMLE_seed" + str(seed) + ".csv", index = False)

