#!/bin/bash

# Must activate correct python environment

for ((seed=9; seed<=20; seed++ ))
do

# Run initial fit, write input to file
echo "Running initial surrogate fit"
#R CMD BATCH "--args seed=$seed" gpMCMC_initial.R &
python gpMLE_initial.py $seed
wait
echo "Initial surrogate fit done"

for (( acq=6; acq<=30; acq++ ))
do
  echo "Running acquisition "
  echo $acq

  # Evaluate simulator at new input
  echo "Evaluating simulator"
  cd ../run_simulator/
  python turbine_call.py &
  wait
  echo "Simulator done"

  # Update surrogate and select new point
  echo "Updating the surrogate"
  cd ../seq_design/
  #R CMD BATCH "--args seed=$seed" gpMCMC_update.R &
  python gpMLE_update.py $seed
  wait
  echo "Surrogate done"
done
wait
done
