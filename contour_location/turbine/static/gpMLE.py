
import numpy as np
import sys
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.gaussian_process.kernels import Matern
import properscoring as ps
import pandas as pd

bounds = [[201160, 698390], [301460, 598020]]
limit = 249280870

def scale_down(x):
    for i in range(x.shape[1]):
        x[:, i] = (x[:, i] - bounds[i][0]) / (bounds[i][1] - bounds[i][0])
    return x

def scale_up(x):
    for i in range(x.shape[1]):
        x[:, i] = x[:, i] * (bounds[i][1] - bounds[i][0]) + bounds[i][0]
    return x

# Read command line arguments -------------------------------------------------

seed = 1
#seed = int(sys.argv[1])
np.random.seed(seed)

# Read data, get train/test split ---------------------------------------------

# Training data
train_dat = np.loadtxt("../data/lhs500.csv", delimiter = ",", skiprows = 1)
train = np.loadtxt("../data/train_seed" + str(seed) + ".csv", skiprows = 1, dtype = int) - 1
x = train_dat[train, :][:, [0, 1]]
x = scale_down(x)
y = train_dat[train, 2]
y_mean = np.mean(y)
y_sd = np.std(y)
y = (y - y_mean) / y_sd

# Testing data
test_dat = np.loadtxt("../data/lhs1000.csv", delimiter = ",", skiprows = 1)
fail = (test_dat[:, 2] > limit)
xp = test_dat[fail, :][:, [0,1]]
xp = scale_down(xp)
yp = test_dat[fail, 2]

# Fit GP ----------------------------------------------------------------------

kernel = 1 * Matern(length_scale = [0.1, 0.1], length_scale_bounds = (0.001, 10), 
                    nu = 2.5)
gp = GPR(kernel, alpha = 1e-6)
gp.fit(x, y)

# Predict at testing set
mu = gp.predict(xp)
mu = (mu * y_sd) + y_mean
sens = np.mean(mu > limit)

results = pd.read_csv("static_results.csv")
new_result = pd.DataFrame(data = {"MODEL": "gpMLE", "SEED": seed, "SENS": [sens]})
results = pd.concat((results, new_result))
results.to_csv("static_results.csv", index = False)
