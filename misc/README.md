# Miscellaneous Examples

This directory contains additional code illustrating the [deepgp](https://cran.r-project.org/web/packages/deepgp/index.html) package on CRAN.  

* **demo.R**: software demo, used in seminars
* **warping_demo.R**: example of the DGP as a spatial warping, used in lectures
