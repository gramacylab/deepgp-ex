set.seed(1)

library(deepgp)

# 1D Higdon Function Set Up ---------------------------------------------------

f <- function(x) {
  i <- which(x <= 0.48)
  y <- vector(length = length(x))
  y[i] <- 2 * sin(pi * x[i] * 4) + 0.4 * cos(pi * x[i] * 16)
  y[-i] <- 2 * x[-i] - 1
  return(y)
}

# Training data
n <- 30
x <- matrix(seq(0, 1, length = n))
y <- f(x) + rnorm(n, 0, 0.01)

# Testing data
N <- 100
xx <- matrix(seq(0, 1, length = N))
yy <- f(xx)

# Visualize true surface
plot(xx, yy, type = "l", xlab = "X", ylab = "Y", main = "Training/Testing Data")
points(x, y, pch = 20, col = 2)

# Fit one-layer GP ------------------------------------------------------------

# Conduct MCMC sampling
fit1 <- fit_one_layer(x, y, nmcmc = 5000, cov = "exp2") # exp2 required for IMSE

# Investigate trace plots
plot(fit1)

# Cut off burn in and thin samples
fit1 <- trim(fit1, 3000, 4)

# Predict across testing set
fit1 <- predict(fit1, xx)
plot(fit1)

# Fit two-layer GP ------------------------------------------------------------

# Conduct MCMC sampling
fit2 <- fit_two_layer(x, y, nmcmc = 5000, cov = "exp2") # exp2 required for IMSE

# Investigate trace plots
plot(fit2) 

# Cut off burn in and thin samples
fit2 <- trim(fit2, 3000, 4)

# Predict across testing set
fit2 <- predict(fit2, xx, store_latent = TRUE) # store_latent used for IMSE later
plot(fit2, hidden = TRUE) # toggle left in plot window to see hidden layer plot

# Calculate IMSE --------------------------------------------------------------

imse1 <- IMSE(fit1, xx)$value
imse2 <- IMSE(fit2, xx)$value

# Plot fits -------------------------------------------------------------------

par(mfrow = c(2, 2))

# Shallow GP
plot(xx, fit1$mean, type = "l", col = 4, xlab = "X", ylab = "Y", main = "One layer GP",
     ylim = c(-2.8, 2.8))
lines(xx, fit1$mean - 2 * sqrt(fit1$s2), col = 4, lty = 2)
lines(xx, fit1$mean + 2 * sqrt(fit1$s2), col = 4, lty = 2)
points(x, y, pch = 20, col = 2)

# Deep GP
plot(xx, fit2$mean, type = "l", col = 4, xlab = "X", ylab = "Y", main = "Two layer GP",
     ylim = c(-2.8, 2.8))
lines(xx, fit2$mean - 2 * sqrt(fit2$s2), col = 4, lty = 2)
lines(xx, fit2$mean + 2 * sqrt(fit2$s2), col = 4, lty = 2)
points(x, y, pch = 20, col = 2)

# Shallow IMSE
plot(xx, imse1, type = "l", xlab = "X", ylab = "ALC", main = "One layer IMSE (minimize)")

# Deep IMSE
plot(xx, imse2, type = "l", xlab = "X", ylab = "ALC", main = "Two layer IMSE (minimize)")

dev.off()
