
# Examples for monotonic warpings in additive and deep Gaussian processes

Maintainer: [Steven D. Barnett](https://www.sdbarnett.github.io) (<sdbarnett@vt.edu>)

This folder contains all the examples from the following paper:

Barnett, S. D., Beesley, L.J., Booth, A.S, Gramacy, R. B. & Osthus, D. (2024). Monotonic warpings for additive and deep Gaussian process. *In review. arXiv:2408.01540*

* **additive**: contains scripts for benchmarking results on additive GPs
* **crosstray**: contains all scripts for the benchmarking of the cross-in-tray function
* **fo_approx.R**: implements linear interpolation found in R approx, but with fixed input and predictive locations
* **fo_approx_init.cpp**: precalculates nearest indices of fixed order grid for linear interpolation
* **lgbb**:  contains all scripts for the benchmarking of the Langley Glide-Back-Booster experiment
* **lineqGPR.R**: code provided from Andrés López-Lopera (https://arxiv.org/abs/2205.08528)
* **michalewicz**: contains all scripts for the benchmarking of the Michalewicz function
* **monoGP.R**: code implementing monoGP for additive Gaussian processes
* **satdrag**: contains all scripts for the benchmarking of the GRACE satellite drag experiment
* **timing**: contains scripts to evaluate and compare computation times (Figure 19)
* **viz.R**: contains code for multiple visualizations included in the paper (Figs. 2-5, 16)
