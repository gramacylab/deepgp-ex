
# Additive GP Benchmarking

This folder contains examples for the additive GPs:

* **arctan.R**: contains code for benchmarking of arctan (https://openreview.net/pdf?id=YCPmfirAcc) (Fig. 21)
* **logit1.R**: contains code for 1d logit example (similar to https://arxiv.org/pdf/2209.00628)
* **logit1_ng.R**: code evaluating effect of fixed grid size on performance (1d logit example) (Fig. 14)
* **logit2.R**: precalculates nearest indices of fixed order grid for linear interpolation (Figs. 6, 7, 8)
* **lopez5.R**:  contains code for benchmarking of Lopez-Lopera arctan example (https://openreview.net/pdf?id=YCPmfirAcc) (Figs. 9, 10, 11)
* **lopez5_ng.R**: code evaluating effect of fixed grid size on performance (5d Lopez-Lopera) (Fig. 15)
