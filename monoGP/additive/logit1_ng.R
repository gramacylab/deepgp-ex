setwd("../")
source("monoGP.R")
source("lineqGPR.R")
setwd("additive")

## Bobby: I made up this example myslef, but then later found
## this paper (https://arxiv.org/pdf/2209.00628) where they
## look at a logistic response surface in 1d.  There is also
## a 2d example (monoGP_logit2.R).  The other examples in that
## paper have a heteroskedastic noise aspect that complicates
## things but as far as I can tell they are all 1d

## Another paper that seems to have only 1d examples:
## http://proceedings.mlr.press/v108/ustyuzhaninov20a/ustyuzhaninov20a.pdf

## Yet another, but also uses derivative info:
## http://proceedings.mlr.press/v9/riihimaki10a/riihimaki10a.pdf

##
## Begin testing code
##

## possibly repeat to get score distribution


## test problem
n <- 20
x <- seq(0, 1, length=n)
nn <- 100
xx <- seq(0.01, 0.99, length=nn)  ## extrapolation not possible now b/c approx
ztrue <- exp(10*x - 5)/(1 + exp(10*x - 5))
zztrue <- exp(10*xx - 5)/(1 + exp(10*xx - 5))
s2true <- 1
nutrue <- 10
## y <- (ztrue - 0.5)*nutrue + rnorm(n, 0, sqrt(s2true))
## yytrue <- (zztrue - 0.5)*nutrue
y <- ztrue*nutrue + rnorm(n, 0, sqrt(s2true))
yytrue <- zztrue*nutrue
yy <- yytrue + rnorm(nn, 0, sqrt(s2true))

## fit and predict
ng <- 10
tic <- proc.time()[3]
fit <- fit_monoGP(x, y, xg=seq(0, 1, length=ng), T=10000)
fit <- trim_monoGP(fit)
pdf(paste0("logit1d_ng", ng, ".pdf"), width=4, height=4.1)
pm <- pred_monoGP(fit, xx)
time <- proc.time()[3] - tic
plot(xx, pm$moms$mean, lwd=2, type="l", xlab="x", ylab="y")
lines(xx, pm$moms$qu, lwd=2, lty=2)
lines(xx, pm$moms$ql, lwd=2, lty=2)
rmse <- sqrt(mean((pm$moms$mean - yytrue)^2))
legend("bottomright", c(paste0("ng = ", ng),
                        paste0("rmse = ", round(rmse, 4)),
                        paste0("secs = ", round(time, 1))), bty="n")
dev.off()

ng <- 20
tic <- proc.time()[3]
fit <- fit_monoGP(x, y, xg=seq(0, 1, length=ng), T=10000)
fit <- trim_monoGP(fit)
pdf(paste0("logit1d_ng", ng, ".pdf"), width=4, height=4.1)
pm <- pred_monoGP(fit, xx)
time <- proc.time()[3] - tic
plot(xx, pm$moms$mean, lwd=2, type="l", xlab="x", ylab="y")
lines(xx, pm$moms$qu, lwd=2, lty=2)
lines(xx, pm$moms$ql, lwd=2, lty=2)
rmse <- sqrt(mean((pm$moms$mean - yytrue)^2))
legend("bottomright", c(paste0("ng = ", ng),
                        paste0("rmse = ", round(rmse, 4)),
                        paste0("secs = ", round(time, 1))), bty="n")
dev.off()

ng <- 50
tic <- proc.time()[3]
fit <- fit_monoGP(x, y, xg=seq(0, 1, length=ng), T=10000)
fit <- trim_monoGP(fit)
pm <- pred_monoGP(fit, xx)
time <- proc.time()[3] - tic
pdf(paste0("logit1d_ng", ng, ".pdf"), width=4, height=4.1)
plot(xx, pm$moms$mean, lwd=2, type="l", xlab="x", ylab="y")
lines(xx, pm$moms$qu, lwd=2, lty=2)
lines(xx, pm$moms$ql, lwd=2, lty=2)
rmse <- sqrt(mean((pm$moms$mean - yytrue)^2))
legend("bottomright", c(paste0("ng = ", ng),
                        paste0("rmse = ", round(rmse, 4)),
                        paste0("secs = ", round(time, 1))), bty="n")
dev.off()

ng <- 100
tic <- proc.time()[3] - tic
fit <- fit_monoGP(x, y, xg=seq(0, 1, length=ng), T=10000)
fit <- trim_monoGP(fit)
pm <- pred_monoGP(fit, xx)
time <- proc.time()[3] - tic
pdf(paste0("logit1d_ng", ng, ".pdf"), width=4, height=4.1)
plot(xx, pm$moms$mean, lwd=2, type="l", xlab="x", ylab="y")
lines(xx, pm$moms$qu, lwd=2, lty=2)
lines(xx, pm$moms$ql, lwd=2, lty=2)
rmse <- sqrt(mean((pm$moms$mean - yytrue)^2))
legend("bottomright", c(paste0("ng = ", ng),
                        paste0("rmse = ", round(rmse, 4)),
                        paste0("secs = ", round(time, 1))), bty="n")
dev.off()