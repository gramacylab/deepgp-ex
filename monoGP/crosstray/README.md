
# Cross-in-tray Example

Code to reproduce bakeoff for cross-in-tray function
(http://www.sfu.ca/~ssurjano/cross) found in Figure 13:

* **crosstray.R**: runs benchmarking on a single training set on the
 cross-in-tray function for GP, TGP, DGP, and mw-DGP. Saves results in a unique
 ".rds" file. For visuals found in Figure 12, set viz=TRUE
* **crosstray.sh**: kicks off multiple runs of crosstray.R (Usage: crosstray.sh
 \[MC reps\])
* **crosstray_collect.R**: gathers ".rds" files and combines results into "csv"
 and generates boxplots (Figure 13)
