
# Langley Glide-Back Booster Computer Experiment Example

Code to reproduce bakeoff for the `lgbb` example found in Figure 13:

This code relies on data that can be downloaded at
<https://bitbucket.org/gramacylab/tpm>. To use this code as-is, download the
data and place it in the home directory and rename it `lgbb`.

* **lgbb.R**: runs benchmarking on a single training set on the
 lgbb computer experiment for GP, TGP, DGP, and mw-DGP. Saves results in a
 unique ".rds" file.
* **lgbb.sh**: kicks off multiple runs of lgbb.R (Usage:
 lgbb.sh \[MC reps\])
* **lgbb_collect.R**: gathers ".rds" files and combines results into "csv"
 and generates boxplots (Figure 13)
