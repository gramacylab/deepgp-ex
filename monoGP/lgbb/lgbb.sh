#!/bin/bash

if ! [[ $1 =~ ^[0-9]+$ ]];
then
  echo "Argument 1 must be integer. Indicates number of Monte Carlo repetitions."
  echo "Usage: lgbb.sh [MC reps]"
  exit 1
fi

echo "Running $1 instances of lgbb example in parallel"
for (( i=1; i<=$1; i++ ))
do
  R CMD BATCH "--args seed=$i" lgbb.R lgbb_$i.Rout &
done

# Wait for all instances to finish before collecting results
wait

# Collect all the results
R CMD BATCH lgbb_collect.R
