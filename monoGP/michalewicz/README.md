
# Michalewicz Example

Code to reproduce bakeoff for "Michalewicz" function
(http://www.sfu.ca/~ssurjano/michalewicz) found in Figure 18:

* **michalewicz.R**: runs benchmarking on a single training set on the
 "Michalewicz" function for GP, TGP, DGP, and mw-DGP. Saves results in a unique
 ".rds" file.
* **michalewicz.sh**: kicks off multiple runs of michalewicz.R (Usage:
 michalewicz.sh \[MC reps\])
* **michalewicz_collect.R**: gathers ".rds" files and combines results into "csv"
 and generates boxplots (Figure 13)
