library(mvtnorm)
library(deepgp)
library(laGP)

## read in updated approx
source("fo_approx.R")

## define a small number
eps <- sqrt(.Machine$double.eps)

## monowarp.ref:
##
## return the monotone adjust warping w(x) based on
## linear interpolation of a (gridded) monotone adjusted
## reference warping from wg(xg) generated generated

monowarp.ref <- function(x, xg, wg, fo_idx=NULL)
{
  ## matrix for separable
  if(is.null(ncol(wg)) || is.null(ncol(x))) {
    wg <- matrix(wg, ncol=1)
    x <- matrix(x, ncol=1)
    vec = TRUE
  } else { vec = FALSE }
  m <- ncol(wg)
  w <- matrix(NA, nrow=nrow(x), ncol=m)

  ## do each column separately
  for(j in 1:m) {
    wg[,j] <- exp(wg[,j])
    wg[,j] <- cumsum(wg[,j])
    r <- range(wg[,j])
    wg[,j] <- wg[,j] - r[1]
    wg[,j] <- wg[,j] / (r[2] - r[1])
    if(is.null(fo_idx)) { w[,j] <- approx(xg, wg[,j], x[,j], rule=2)$y
    } else { w[,j] <- fo_approx(xg, wg[,j], x[,j], fo_idx[,j]) }
  }

  ## un-matrix for non-separable
  if(vec) w <- as.numeric(w)

  return(w)
}


## gpmargllik:
##
## GP mrginal log likelihood for latent Z in [0,1]
## variable where the latent function f is derived from
## centering Z at zero (generalize later) and scaling it
## by nu
##
## largely follows the dynaTree marginal likelihood calcs

gpmargllik <- function(y, z, nu)
  {
    if(is.null(ncol(z))) z <- matrix(z, ncol=1)
    f <- (z - 0.5) %*% nu

    ## Integrating over mu and s2
    n <- length(y)
    mu <- mean(y - f)
    s2 <- sum((y - mu - f)^2)     ## no divide by n for llik
    ll <- -((n-1)/2) * log(s2/2)  ## dropping constants of prop

    return(ll)
}


## ess_sample:

## copied from deepgp-git/presentations/ess_demo/solution.R
## and modified for the monogp application using a reference
## grid; completes one iteration of elliptical slice sampling
## for zg on grid xg

ess_sample <- function(j, y, x, zg_prev, xg, theta, nu, fo_idx=NULL)
  {
    ## because of automatic dropping
    if(is.null(ncol(zg_prev))) zg_prev <- matrix(zg_prev, ncol=1)

    ## Calculate log likelihood of previous sample
    z_prev <- monowarp.ref(x, xg, zg_prev, fo_idx)
    logl_prev <- gpmargllik(y, z_prev, nu)

    ## Draw from prior distribution
    S <- exp(-Dg / theta) + diag(eps, nrow(Dg))
    zg_prior <- drop(rmvnorm(1, sigma=S))
    ## zg_prior <- drop(rmvnorm(1, sigma=Sigma(xg, theta=theta)))

    ## Initialize a, bounds on a, and random uniform draw
    a <- runif(1, min=0, max=2*pi)
    amin <- a - 2*pi
    amax <- a
    u <- runif(1, min=0, max=1)

    ## ESS loop: propose values and repeat until acceptance is reached
    accept <- FALSE
    z_new <- z_prev
    while (accept == FALSE) {

      ## Calculate proposal
      zg_new_j <- zg_prev[,j]*cos(a) + zg_prior*sin(a)

      ## evaluate log likelihood after monowarping
      if(is.null(fo_idx)) { z_new[,j] <- monowarp.ref(x[,j], xg, zg_new_j, NULL)
      } else { z_new[,j] <- monowarp.ref(x[,j], xg, zg_new_j, fo_idx[,j,drop=FALSE]) }
      logl_new <- gpmargllik(y, z_new, nu)

      ## Accept or reject
      if (logl_new > logl_prev + log(u)) {
        accept <- TRUE
      } else {
        ## Update the bounds on a and repeat
        if (a < 0) {
          amin <- a
        } else {
          amax <- a
        }
        a <- runif(1, amin, amax)
      } ## end of else statement
  } ## end of while loop

  return(zg_new_j)
}


## mh.nu:
##
## Metropolis-Hastings sample for the scale parameter nu

mh.nu <- function(j, y, x, zg, xg, nu_prev, ab=NULL, fo_idx=NULL)
  {
    ## warp latents
    z <- monowarp.ref(x, xg, zg, fo_idx)

    ## unif propose pos
    nu_new <- nu_prev
    nu_new[j] <- runif(1, nu_prev[j]/2, 2*nu_prev[j])

    ## likelihoods
    logl_prev <- gpmargllik(y, z, nu_prev)
    logl_new <- gpmargllik(y, z, nu_new)

    ## priors
    lp_prev <- dgamma(nu_prev[j], ab[1], ab[2], log=TRUE)
    lp_new <- dgamma(nu_new[j], ab[1], ab[2], log=TRUE)

    ## mh accept-reject
    la <- logl_new + lp_new - logl_prev - lp_prev + nu_prev[j] - nu_new[j]
    if(la > log(runif(1))) {
      return(nu_new[j])       ## accept
    } else {
      return(nu_prev[j])      ## reject
    }
}


## mh.theta:
##
## Metropolis-Hastings sample for the lengthscale theta

mh.theta <- function(zg, xg, theta_prev, ab)
{
  ## unif propose pos
  theta_new <- runif(1, theta_prev/2, 2*theta_prev)

  ## likelihoods
  S <- exp(-Dg / theta_prev) + diag(eps, nrow(Dg))
  logl_prev <- dmvnorm(zg, sigma=S, log=TRUE)
  S <- exp(-Dg / theta_new) + diag(eps, nrow(Dg))
  logl_new <- dmvnorm(zg, sigma=S, log=TRUE)
  # logl_prev <- dmvnorm(zg, sigma=Sigma(xg, theta=theta_prev), log=TRUE)
  # logl_new <- dmvnorm(zg, sigma=Sigma(xg, theta=theta_new), log=TRUE)

  ## priors
  lp_prev <- dgamma(theta_prev, ab[1], ab[2], log=TRUE)
  lp_new <- dgamma(theta_new, ab[1], ab[2], log=TRUE)

  ## mh accept-reject
  la <- logl_new + lp_new - logl_prev - lp_prev + theta_prev - theta_new
  if(la > log(runif(1))) { return(theta_new)
  } else { return(theta_prev) }
}


## fit_monoGP:
##
## MCMC function for sampling from the posterior of the monoGP
## model
##
## Note, model assume zero-mean response (y), so it is important
## to pre-center.  This is much more important for monoGP than
## another, more general GP due to the way the latent function
## space is integrated

fit_monoGP <- function(x, y, xg=seq(0, 1, length=49), T=5000, use_fo=FALSE, verb=1000)
  {
    ## make space for sample and assign initial values,
    ## hard-coded for now
    ng <- length(xg)
    if(is.null(dim(x))) x <- matrix(x, ncol=1)
    m <- ncol(x)
    zgs <- array(NA, dim=c(T, ng, m))
    for(j in 1:m) zgs[1,,j] <- xg
    thetas <- nus <- matrix(NA, nrow=T, ncol=m)
    nus[1,] <- 1
    thetas[1,] <- 0.1

    ## build fo_idx
    if(use_fo) {
      fo_idx <- fo_approx_init(xg, x)
    } else fo_idx <- NULL

    ## precalculate distances and avoid passing to functions
    Dg <<- distance(xg)

    ## MCMC iterations
    for (t in 2:T) {

      ## progress meter
      if(verb > 0 && t %% verb == 0) cat("t=", t, "\n", sep="")

      ## MH-within-Gibbs, start with previous iteration
      zgs[t,,] <- zgs[t-1,,]
      nus[t,] <- nus[t-1,]
      thetas[t,] <- thetas[t-1,]

      ## then loop over coordinates
      for(j in 1:m) {
        ## ESS for z
        zgs[t,,j] <- ess_sample(j, y, x, zgs[t,,], xg, thetas[t,j], nus[t,], fo_idx=fo_idx)
        ## MH for nu
        nus[t,j] <- mh.nu(j, y, x, zgs[t,,], xg, nus[t,], ab=c(0.001,0.001), fo_idx=fo_idx)
        ## MH for theta
        thetas[t,j] <- mh.theta(zgs[t,,j], xg, thetas[t,j], ab=c(3/2,5))
        ## cat("t=t", t, ", j=", j, ", theta=", thetas[t,j], "\n")
      }
    }

    ## build object to send back
    return(list(zg=zgs, nu=nus, theta=thetas, xg=xg, x=x, y=y, fo_idx=fo_idx))
  }


## trim_monoGP:
##
## discard burn-in (B) and sub-sample every E to
## thin the chain

trim_monoGP <- function(x, B=1000, E=10)
  {
    x$zg <- x$zg[-(1:B),,,drop=FALSE]
    x$zg <- x$zg[seq(1, nrow(x$zg), by=E),,,drop=FALSE]
    x$nu <- x$nu[-(1:B),,drop=FALSE]
    x$nu <- x$nu[seq(1, nrow(x$nu), by=E),,drop=FALSE]
    x$theta <- x$theta[-(1:B),,drop=FALSE]
    x$theta <- x$theta[seq(1, nrow(x$theta), by=E),,drop=FALSE]

    return(x)
  }


## pred_monoGP:
##
## sample from the posterior prediction at xx given a monoGP fit

pred_monoGP <- function(fit, xx, level=0.95, lite=FALSE, use_fo=FALSE)
  {
    ## allocate space
    if(is.null(ncol(xx))) xx <- matrix(xx,)
    nn <- nrow(xx)
    n <- length(fit$y)
    T <- nrow(fit$zg)
    ql <- qu <- ff <- mup <- matrix(NA, nrow=T, ncol=nn)
    s2p <- rep(NA, T)
    q <- qt(1 - (1 - level)/2, n-1)

    ## build fo_idx
    if(use_fo) {
      fo_idx <- fo_approx_init(fit$xg, xx)
    } else fo_idx <- NULL

    ## loop over MCMC iterations to calculation samplee
    for(t in 1:T) {

      ## get monowarped predictive latents
      zz <- monowarp.ref(xx, fit$xg, fit$zg[t,,], fo_idx)
      if(is.null(nrow(zz))) zz <- matrix(zz, ncol=1)
      ff[t,] <- (zz - 0.5) %*% fit$nu[t,]

      ## estimate residual variance
      z <- monowarp.ref(fit$x, fit$xg, fit$zg[t,,], fit$fo_idx)
      if(is.null(nrow(z))) z <- matrix(z, ncol=1)   ## maybe put in monowarp.ref
      f <- (z - 0.5) %*% fit$nu[t,]
      mu <- mean(fit$y - f)
      ## not sure about this, from dynaTree
      s2p[t] <- (1 + 1/n) * sum((fit$y - mu - f)^2) / (n - 1)
      mup[t,] <- mu + ff[t,]
      ## calculate quantiles
      ql[t,] <- mup[t,] - q*sqrt(s2p[t])
      qu[t,] <- mup[t,] + q*sqrt(s2p[t])
    }

    ## summarize predictive moments, beginning with means
    moms <- data.frame(mean=colMeans(mup), qu=colMeans(qu), ql=colMeans(ql))

    ## var/cov requires a correction to s2p which assumes student-t and
    ## also includes variance of the mean, but we calculate that separately here
    nd <- (n - 1) / ((1 + 1/n) * (n - 3))

    ## depending on
    if(lite) {  ## only variances (diagonal)
      s2 <- apply(mup, 2, var) + mean(s2p)*nd
      Spred <- NULL
    } else {  ## full covariance structure
      Spred <- cov(mup) + diag(mean(s2p)*nd, nn)
      s2 <- NULL
    }

    ## return moments and samples
    return(list(samps=list(ff=ff, qu=qu, ql=ql, mup=mup, s2p=s2p),
      moms=moms, S=Spred, s2=s2))
}


## latent_monoGP:
##
## return the latent structure in each dimension of a monoGP fit;
## optional whether to apply nu from the posterior samples

latent_monoGP <- function(fit, nu=FALSE, use_fo=FALSE)
  {
    ## allocate space and determine quantiles
    n <- length(fit$y)
    T <- nrow(fit$zg)
    m <- ncol(fit$x)
    ff <- array(NA, dim=dim(fit$zg))

    ## build fo_idx
    if(use_fo) {
      fo_idx <- fo_approx_init(fit$xg, fit$xg)
    } else fo_idx <- NULL

    ## loop over MCMC iterations to calculation samplee
    for(t in 1:T) {

      ## loop over input coordinates
      for(j in 1:m)  {
        if(is.null(fo_idx)) { zz <- monowarp.ref(fit$xg, fit$xg, fit$zg[t,,j])
        } else { zz <- monowarp.ref(fit$xg, fit$xg, fit$zg[t,,j], fo_idx[,j]) }
        if(nu) { ff[t,,j] <- (zz - 0.5) * fit$nu[t,j]
        } else { ff[t,,j] <- (zz - 0.5) }
      }
    }

    ## return moments and samples
    return(ff)
}


## score:
##
## standard out of sample metric using mean and variance

## now getting from deepgp

# score <- function(Y, mu, Sigma, mah=FALSE)
#   {
#     Ymmu <- Y - mu
#     Sigmai <- solve(Sigma)
#     mahdist <- t(Ymmu) %*% Sigmai %*% Ymmu
#     if(mah) return(sqrt(mahdist))
#     return (- determinant(Sigma, logarithm=TRUE)$modulus - mahdist)
#   }

