
# Satellite Drag Computer Experiment Example

This code relies on the public repository,
<https://bitbucket.org/gramacylab/tpm>. To use this code as-is, clone this
repository in the home directory and rename it `tpm-git`. Compile C code by
running `R CMD SHLIB -o tpm.so *.c` in the `tpm-git/tpm/src` directory.

Additionally, training and test sets need to be created and stored in a
directory names `satdrag_data`. This can be done by running the script
`create_sat_drag_data.R`.

* **create_sat_drag_data.R**: runs benchmarking on a single training set on the
 "Michalewicz" function for GP, TGP, DGP, and mw-DGP. Saves results in a unique
 ".rds" file.
* **satdrag.R**: runs benchmarking on a single training set on the
 "Michalewicz" function for GP, TGP, DGP, and mw-DGP. Saves results in a unique
 ".rds" file.
* **satdrag.sh**: kicks off multiple runs of satdrag.R (Usage:
 satdrag.sh \[MC reps\])
* **satdrag_collect.R**: gathers ".rds" files and combines results into "csv"
 and generates boxplots (Figure 13)
