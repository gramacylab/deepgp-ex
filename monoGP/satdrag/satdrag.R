setwd("../")
source("monoGP.R")
setwd("satdrag")

library(lhs)
library(deepgp)
library(tgp)

## https://bookdown.org/rbg/surrogates/chap2.html#chap2satdrag
## GRACE satellite drag example

seed <- 0
fn <- 1

## read in the command line arguments
## run with: R CMD BATCH '--args seed=1 fn=1' satdrag.R
args <- commandArgs(TRUE)
if (length(args) > 0) {
  for(i in 1:length(args)) {
    eval(parse(text=args[[i]]))
  }
}
settings <- list(seed=seed, fn=fn, pap=pap)

## Parameter limits
lims <- matrix(c(5500, 9500, 100, 500, 200, 2000, 0, 0.06, -0.06, 0.06,
  0, 1, 0, 1), nrow=2, byrow=FALSE)
ranges <- abs(lims[2,]-lims[1,])

## List all files
files <- list.files(path="satdrag_data",
  pattern="train_satdrag_[0-9]*.csv")
## Load training set of 550 runs, with 50 replicated
train <- read.csv(paste0("satdrag_data/", files[fn]))
## Load testing set of 1000 runs
test <- read.csv(paste0("satdrag_data/", "test_satdrag.csv"))

X <- as.matrix(train[,-ncol(train)])
Y <- train[,ncol(train)]

XX <- as.matrix(test[,-ncol(test)])
YY <- test[,ncol(test)]
Yall <- c(Y, YY)
Y <- (Y - mean(Yall))/sd(Yall)
YY <- (YY - mean(Yall))/sd(Yall)

for (i in 1:ncol(X)) {
  X[,i] <- (X[,i] - lims[1,i])/ranges[i]
  XX[,i] <- (XX[,i] - lims[1,i])/ranges[i]
}
colnames(X) <- colnames(XX) <- NULL

## Set parameters to match Sauer et. al (2022)
m <- 25
trueg <- NULL
g0 <- 1e-5
theta0 <- thetay0 <- 0.5
thetaw0 <- rep(1, ncol(X))
vec <- FALSE
nmcmc <- 10000
burnin <- 1000
thin <- 10
w0 <- NULL

rmses <- crps <- fit_times <- pred_times <- rep(NA, 4)
names(rmses) <- names(crps) <- names(fit_times) <- names(pred_times) <-
  c("GP", "TGP", "DGP", "mw-DGP")

## fit gaussian process using deepgp package (one-layer) and make predictions
## at XX
tic <- proc.time()[3]
gpfit <- deepgp::trim(fit_one_layer(x=X, y=Y, true_g=trueg, g_0=g0,
 theta_0=theta0, nmcmc=nmcmc, sep=TRUE, vecchia=vec, m=m, verb=TRUE),
 burn=burnin, thin=thin)
toc <- proc.time()[3]
fit_times[1] <- toc-tic
tic <- proc.time()[3]
gppreds <- predict(gpfit, x_new=XX)
toc <- proc.time()[3]
rmses[1] <- sqrt(mean((gppreds$mean-YY)^2))
crps[1] <- deepgp::crps(y=YY, mu=gppreds$mean, s2=gppreds$s2)
pred_times[1] <- toc-tic
print("Finished predictions for ordinary GP")

## make predictions using tgp at XX
### create new directory for tgp to run so as not to overwrite files
temp_dir <- paste0("lgbb_tgp_", seed)
dir.create(temp_dir)
setwd(temp_dir)
tic <- proc.time()[3]
tgpfit <- btgpllm(X=X, Z=Y, XX=XX, pred.n=FALSE)
toc <- proc.time()[3]
fit_times[2] <- toc-tic
rmses[2] <- sqrt(mean((tgpfit$ZZ.mean-YY)^2))
crps[2] <- deepgp::crps(y=YY, mu=tgpfit$ZZ.mean, s2=tgpfit$ZZ.s2)
setwd("../")
unlink(temp_dir, recursive=TRUE)
print("Finished predictions for treed GP")

## make deep gp predictions at XX
tic <- proc.time()[3]
dgpfit <- deepgp::trim(fit_two_layer(x=X, y=Y, true_g=trueg, g_0=g0,
 theta_y_0=thetay0, theta_w_0=rep(1, ncol(X)), nmcmc=nmcmc, vecchia=vec, m=m,
 verb=TRUE), burn=burnin, thin=thin)
toc <- proc.time()[3]
fit_times[3] <- toc-tic
tic <- proc.time()[3]
dgppreds <- predict(dgpfit, x_new=XX)
toc <- proc.time()[3]
rmses[3] <- sqrt(mean((dgppreds$mean-YY)^2))
crps[3] <- deepgp::crps(y=YY, mu=dgppreds$mean, s2=dgppreds$s2)
pred_times[3] <- toc-tic
print("Finished predictions for deepgp")

## make monowarped deep gp predictions at XX
tic <- proc.time()[3]
mdgpfit <- deepgp::trim(fit_two_layer(x=X, y=Y, true_g=trueg, g_0=g0, w_0=w0,
  theta_y_0=thetay0, theta_w_0=thetaw0, nmcmc=nmcmc, monowarp=TRUE,
  vecchia=vec, m=m, verb=TRUE), burn=burnin, thin=thin)
toc <- proc.time()[3]
fit_times[4] <- toc-tic
tic <- proc.time()[3]
mdgppreds <- predict(mdgpfit, x_new=XX)
toc <- proc.time()[3]
rmses[4] <- sqrt(mean((mdgppreds$mean-YY)^2))
crps[4] <- deepgp::crps(y=YY, mu=mdgppreds$mean, s2=mdgppreds$s2)
pred_times[4] <- toc-tic
print("Finished predictions for mw-DGP")

saveRDS(list(rmses=rmses, crps=crps, fit_times=fit_times,
  pred_times=pred_times), paste0("satdrag_results_", seed, ".rds"))
