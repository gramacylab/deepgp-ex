#!/bin/bash

# Read in number of unique training data files
ncopies=($( find satdrag_data -name train_satdrag_* | wc -l))

echo "Running $ncopies instances of satellite drag example in parallel"
for (( i=1; i<=$ncopies; i++ ))
do
  R CMD BATCH "--args seed=$i fn=$i" satdrag.R satdrag_$i.Rout &
done

# Wait for all instances to finish before collecting results
wait

# Collect all the results
R CMD BATCH satdrag_collect.R
