satdrag_files <- list.files(pattern="satdrag_results_[0-9]*.rds")

res <- readRDS(satdrag_files[1])
rmses_all <- crps_all <- pred_times_all <- fit_times_all <-
 matrix(NA, ncol=length(res$rmses), nrow=length(satdrag_files))
colnames(rmses_all) <- colnames(crps_all) <- colnames(pred_times_all) <-
 colnames(fit_times_all) <- c("GP", "TGP", "DGP", "mw-DGP")

for (fn in 1:length(satdrag_files)) {
  res <- readRDS(satdrag_files[fn])
  rmses_all[fn,] <- res$rmses
  crps_all[fn,] <- res$crps
  fit_times_all[fn,] <- res$fit_times
  pred_times_all[fn,] <- res$pred_times
}

write.csv(rmses_all, paste0('satdrag_rmses.csv'), row.names=FALSE)
write.csv(crps_all, paste0('satdrag_crps.csv'), row.names=FALSE)
write.csv(fit_times_all, paste0('satdrag_fit_times.csv'), row.names=FALSE)
write.csv(pred_times_all, paste0('satdrag_pred_times.csv'), row.names=FALSE)

par(mfrow=c(1,1), mar=c(5.1, 4.1, 1.1, 2.1))
pdf("satdrag_rmse_bp.pdf", width=3, height=4)
boxplot(rmses_all, main="satellite drag", ylab="RMSE", las=2)
dev.off()
pdf("satdrag_crps_bp.pdf", width=3, height=4)
boxplot(crps_all, main="satellite drag", ylab="CRPS", las=2)
dev.off()
