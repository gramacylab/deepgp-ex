
# Measure execution time

Code to evaluate the performance of different methods in terms of speed of execution:

* **timing.R**: evaluating execution time on the 5d Lopez-Lopera problem (Fig. 19)
* **timing_DGP.R**: evaluating execution time for deep GPs on the 3d Michalewicz problem (Fig. 19)
