setwd("../")
source("monoGP.R")
source("lineqGPR.R")
setwd("timing")
library(lhs)
library(laGP)

set.seed(1)  ## to reproduce particular figures

## Bobby: taking an additive approach to monotone GP regression.
##
## Setup is similar to (https://openreview.net/pdf?id=YCPmfirAcc), but
## their inference technique is very different.  Also, they get hung
## up on constrants and on super high dimensional problems.  But
## perhaps I can try one of those problems.

## f:
##
## example is from https://openreview.net/pdf?id=YCPmfirAcc
## it is written with a nu vector for vidsuals, but the default
## recovers the exact function in the paper

f <- function(x, nu=c(1.37, 1.12, 1, 2, 2), s2, center=FALSE)
{
  ## building up component parts
  n <- nrow(x)
  z1 <- atan(5*x[,1])/1.37
  z2 <- atan(2*x[,2])/1.12
  z3 <- x[,3]
  z4 <- x[,4]^2
  z5 <- 1/(1 + exp(-10*(x[,5] - 0.5)))
  z <- cbind(z1, z2, z3, z4, z5)

  ## not essential, but helps when visualizing if you'vee
  ## got the latent right
  if(center) z <- z - 0.5
  z[is.na(z)] <- 0  ## focus on 1d when whole cols of x are 0

  ## linear/additive combination and noise
  y <- z %*% nu
  if(s2 > 0) y <- y + rnorm(n, 0, sqrt(s2))

  return(y)
}

## different training data sizes 2^ep
ep <- 6:21
ftime <- ptime <- plite <- matrix(NA, nrow=length(ep), ncol=4)
colnames(ftime) <- colnames(ptime) <- colnames(plite) <-
  c("mono-GP", "mono-GP-fo", "lineq-GP", "GP")
for(i in 1:length(ep)) {

  ## training data
  n <- round(40 + 1.5^ep[i])
  cat("n = 40+2^", ep[i], " = ", n, "\n", sep="")
  x <- randomLHS(n, 5)
  s2true <- 0.1
  nutrue <- c(1.37, 1.12, 1, 2, 2)  ## this recreates the Lopez function exactly
  y <- f(x, nutrue, s2true)

  ## testing data in 2d grid
  nn <- n
  xx <- randomLHS(nn, 5)
  yytrue <- f(xx, nutrue, 0)
  yy <- f(xx, nutrue, s2true)

  ## mono-GP fit and predict on testing set
  tic <- proc.time()[3]
  fit <- fit_monoGP(x, y, xg=seq(0, 1, length=49), T=5000)
  fit <- trim_monoGP(fit)
  ftime[i,1] <- proc.time()[3] - tic
  tic <- proc.time()[3]
  pm <- pred_monoGP(fit, xx)
  ptime[i,1] <- proc.time()[3] - tic
  tic <- proc.time()[3]
  pml <- pred_monoGP(fit, xx, lite=TRUE)
  plite[i,1] <- proc.time()[3] - tic

  ## mono-GP fit and predict on testing set
  tic <- proc.time()[3]
  fit <- fit_monoGP(x, y, xg=seq(0, 1, length=49), T=5000, use_fo=TRUE)
  fit <- trim_monoGP(fit)
  ftime[i,2] <- proc.time()[3] - tic
  tic <- proc.time()[3]
  pm <- pred_monoGP(fit, xx, use_fo=TRUE)
  ptime[i,2] <- proc.time()[3] - tic
  tic <- proc.time()[3]
  pml <- pred_monoGP(fit, xx, lite=TRUE, use_fo=TRUE)
  plite[i,2] <- proc.time()[3] - tic

  ## laGP
  tic <- proc.time()[3]
  g <- garg(list(mle=TRUE), y)
  d <- darg(NULL, x)
  gpi <- newGPsep(x, y, d=d$start, g=g$start, dK=TRUE)
  mle <- mleGPsep(gpi, param="both", tmin=c(d$min, g$min), tmax=c(d$max, g$max),
                  ab=c(d$ab, g$ab), verb=0)
  ftime[i,4] <- proc.time()[3] - tic
  tic <- proc.time()[3]
  p <- predGPsep(gpi, xx)
  ptime[i,4] <- proc.time()[3] - tic
  tic <- proc.time()[3]
  pl <- predGPsep(gpi, xx, lite=TRUE)
  plite[i,4] <- proc.time()[3] - tic

  ## lineq-GP
  if(n < 4000) {
    mets <- lineq_mets(x, y, xx, yy, yytrue, lite.time=TRUE, skip.score=TRUE)
    ftime[i,3] <- mets[4]
    ptime[i,3] <- mets[6]
    plite[i,3] <- mets[5]
  }

  ## keep track of progress visually
  pdf("timing.pdf", width=5, height=5)
  ## r <- range(ftime[,c(1,3)], na.rm=TRUE)
  plot(ep, ftime[,1], ylim=c(0,1000), type="l", lwd=2, ylab="seconds",
       xlab="n = n' = floor(40 + 1.5^x)",
       main="comparing fit times")
  lines(ep, ftime[,2], col=2, lwd=2, lty=2)
  lines(ep, ftime[,3], col=3, lwd=2, lty=3)
  lines(ep, ftime[,4], col=4, lwd=2, lty=4)
  legend("topleft", colnames(ftime), lwd=2, col=1:4, lty=1:4)
  dev.off()
  ## lines(ep, ftime[,1] + ptime[,1], lwd=2, lty=2)
  ## lines(ep, ftime[,2] + ptime[,2], lwd=2, lty=2, col=2)
  ## lines(ep, ftime[,3] + ptime[,3], lwd=2, lty=2, col=3)
  ## lines(ep, ftime[,1] + plite[,1], lwd=2, lty=3)
  ## lines(ep, ftime[,2] + plite[,2], lwd=2, lty=3, col=2)
  ## lines(ep, ftime[,3] + plite[,3], lwd=2, lty=3, col=3)

  ## save results for plotting later
  save.image("timing.RData")
}
