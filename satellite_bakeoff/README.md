
# Satellite Bake-off

This directory contains the simulation study from the following paper:

Sauer, A., Cooper, A., & Gramacy, R. B. (2023).  Non-stationary Gaussian process surrogates. *To appear in the Handbook of Uncertainty Quantification.* arXiv:2305.19242

This code relies on data from the public repository, <https://bitbucket.org/gramacylab/tpm>.  To 
use this code as-is, clone this repository in the home directory and rename it `tpm-git`.  
Compile C code by running `R CMD SHLIB -o tpm.so *.c` in the `tpm-git/tpm/src`
directory.

* **model_fits:** contains a code script to fit each of the entertained surrogate models
* **plot.R**: generates Figure 5 from the paper
* **predictions**: stores the predicted mean and variance from each model for each of 10 reps
* **prescale_theta.csv**: estimated separable lengthscales, used for pre-scaling inputs
* **prescale.R**: uses GP SVEC model to estimate separable lengthscales for each rep
* **run.sh**: bash script used to run both python and R scripts
* **scaling_functions.R:** functions used to pre-scale the input data
* **train_test**: stores row indices of training/testing splits for each of 10 reps