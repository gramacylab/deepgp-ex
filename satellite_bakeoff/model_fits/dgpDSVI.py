
"""
Evaluatess two-layer DSVI DGP on the 8D satellite drag simulation
Utilizes "gpflux" package, with python wrapper "dsvi.py"
Requires specified seed
"""

import sys
import pandas as pd
import numpy as np
sys.path.append("../vecchia/competitors") # Relative file path from within this repo
import dsvi

def scale_down(X, mins, maxs):
    for i in range(X.shape[1]):
        X[:, i] = (X[:, i] - mins[i]) / (maxs[i] - mins[i])
    return X

def pre_scale(X, theta):
    for i in range(X.shape[1]):
        X[:, i] = X[:, i] / theta[i]
    return X
        
d = 8
seed = 1
np.random.seed(seed)

## Import data
dat = np.loadtxt("~/tpm-git/data/HST/hstHe.dat", delimiter = " ", skiprows = 1)
  # Change this file path to point to the cloned "tpm" repository
bounds_min = np.apply_along_axis(min, 0, dat[:, 0:d])
bounds_max = np.apply_along_axis(max, 0, dat[:, 0:d])

## Get train/test split (convert to zero indexing)
test = np.loadtxt("../train_test/test_index_seed" + str(seed) + ".csv", 
                  skiprows = 1, dtype = int) - 1
train = np.loadtxt("../train_test/train_index_seed" + str(seed) + ".csv", 
                   skiprows = 1, dtype = int) - 1
x = dat[train, 0:d]
y = dat[train, d]
xx = dat[test, 0:d]
yy = dat[test, d]

## Scale response to zero mean, unit variance
y_mean = np.mean(y)
y_sd = np.std(y)
y = (y - y_mean) / y_sd    

## Scale inputs to [0, 1]^d
x = scale_down(x, bounds_min, bounds_max)
xx = scale_down(xx, bounds_min, bounds_max)

## Pre-scale inputs by estimated lengthscales
theta = pd.read_csv("../prescale_theta.csv")["seed" + str(seed)]
x = pre_scale(x, theta)
xx = pre_scale(xx, theta)

## Fit model
mean, s2 = dsvi.DSVI(x, y, xx, num_layers = 2, num_inducing = 100, g = 1e-4)

## Unscale and store predictions
mean = mean * y_sd + y_mean
s2 = s2 * (y_sd**2) 
p = {"truth": yy.reshape(-1), "mean": mean.reshape(-1), "s2": s2.reshape(-1)}
p = pd.DataFrame(data = p)
p.to_csv("../predictions/dgpDSVI_seed" + str(seed) + ".csv")
