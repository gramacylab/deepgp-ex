
# Evaluates laGP on the 8D satellite drag simulation
# Requires specified seed

library(laGP)
source("../scaling_functions.R")

d <- 8
seed <- 1 
args <- commandArgs(TRUE)
if(length(args) > 0) 
  for(i in 1:length(args)) 
    eval(parse(text = args[[i]]))
cat("seed is ", seed, "\n")
set.seed(seed)

## Import data
dat <- read.delim("~/tpm-git/data/HST/hstHe.dat", sep = " ", header = TRUE)
  # Change this file path to point to the cloned "tpm" repository
bounds <- apply(dat[, 1:8], 2, range)

## Get train/test split
test <- read.csv(paste0("../train_test/test_index_seed", seed, ".csv"))[[1]]
train <- read.csv(paste0("../train_test/train_index_seed", seed, ".csv"))[[1]]
x <- as.matrix(dat[train, 1:8])
y <- dat[train, 9]
xx <- as.matrix(dat[test, 1:8])
yy <- dat[test, 9] # does not get scaled
rm(dat, train, test)

## Scale response to zero mean, unit variance
y_mean <- mean(y)
y_sd <- sd(y)
y <- (y - y_mean) / y_sd

## Scale inputs to [0, 1]^d
x <- scale_down(x, bounds)
xx <- scale_down(xx, bounds)

## Pre-scale inputs by estimated lengthscales
theta <- read.csv("../prescale_theta.csv")[, seed]
x <- pre_scale(x, theta)
xx <- pre_scale(xx, theta)

## Fit model
fit <- aGPsep(X = x, Z = y, XX = xx, method = "alc", omp.threads = 8)

## Unscale and store predictions
mean <- fit$mean * y_sd + y_mean
s2 <- fit$var * (y_sd ^ 2)
pred <- data.frame(truth = yy, mean = mean, s2 = s2)
write.csv(pred, file = paste0("../predictions/laGP_seed", seed, ".csv"),
          row.names = FALSE)
  
