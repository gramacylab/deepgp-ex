
# Vecchia-approximated Deep Gaussian Processes

This folder contains all the examples from the following paper:

Sauer, A., Cooper, A., & Gramacy, R. B. (2022).  Vecchia-approximated deep Gaussian processes for computer experiments.  *Journal of Computational and Graphical Statistics,* 1-14.  *arXiv:2204.02904*

* **competitors**: contains files for implementing competing software including function wrappers and virtual environment details
* **g4**: contains work for the 4-dimensional G function (Section 5.1)
* **g4_noise**: contains work for the 4-dimensional G function with additive Gaussian noise (Appendix E.1)
* **g6**: contains work for the 6-dimensional G function (Appendix E.2)
* **m**: contains comparison of predictions for various m values (Appendix D)
* **plots**: contains R scripts used to generate all figures
* **satellite**: contains work for the satellite drag experiment (Section 5.2)
* **schaffer**: contains work for the 2-dimensional Schaffer function (Section 5.1)
* **timing**: contains script to evaluate and compare computation times (Figure 3)
