
# Competing Software

This folder contains details on implementing competing methodologies: DGP DSVI (Salimbeni & Deisenroth, 2017), DGP HMC (Havasi et al., 2018), and DGP RFE (Cutajar et al., 2017).  DGP RFE results were omitted from the paper since they were not competitive.

* **dsvi_env.txt**: contains details on the virtual environment used to run DGP DSVI models (output of `conda list -n dsvi_env --export > dsvi_env.txt`)
* **dsvi.py**: python wrapper for fitting DGP DSVI models using the `gpflux` package
* **hmc_env.txt**: contains details on the virtual environment used to run DGP HMC models (output of `conda list -n hmc_env --export > hmc_env.txt`)
* **rfe_cutajar_env.txt**: contains details on the virtual environment used to run DGP RFE models following Cutajar et al., 2017 (output of `conda list -n rfe_cutajar_env --export > rfe_cutajar_env.txt`)
* **rfe_cutajar.py**: python wrapper for fitting DGP RFE Cutajar models
* **rfe_marmin_env.txt**: contains details on the virtual environment used to run DGP RFE models following Marmin & Filippone, 2022 (output of `conda list -n rfe_marmin_env --export > rfe_marmin_env.txt`)
* **rfe_marmin.py**: python wrapper for fitting DGP RFE Marmin models

## DGP DSVI

DSVI scripts rely on the `gpflux` package (https://secondmind-labs.github.io/GPflux/).
The virtual environment used to run DSVI python scripts is detailed in the `dsvi_environment.txt` file.
You **must** install the `gpflux` package and be in this environment in order to recreate our DSVI (method 1) results.

## DGP HMC

HMC scripts rely on the `sghmc_dgp` github repository (https://github.com/cambridge-mlg/sghmc_dgp).
The virtual environment used to run HMC python scripts is detailed in the `hmc_environment.txt` file.
The repository did not provide details on package versions, so we had to make the following
changes to the source code in order for it to work in our environment:

* In the `models.py` file, comment out line 4 (`from scipy.stats import norm`) and line 5 (`from scipy.misc import logsumexp`)
* In the `kernels.py` file, change `keepdims` to `keep_dims` in lines 168 and 176.

You **must** clone the repo, make these manual changes, and be in the described environment in order to recreate our HMC (method 2) results.

## DGP RFE Cutajar

RFE Cutajar scripts rely on the `deep_gp_random_features` github repository (https://github.com/mauriziofilippone/deep_gp_random_features).  The virtual environment used to run RFE Cutajar scripts is detailed in the `rfe_cutajar_env.txt` file.  
You **must** clone this repo and be in this environment in order to recreate our RFE Cutajar (method 3) results.

## DGP RFE Marmin

RFE Marmin scripts rely on the `variational-calibration` github repository (https://github.com/SebastienMarmin/variational-calibration).  The virtual environment used to run RFE Marmin scripts is detailed in the `rfe_marmin_env.txt` file.  
You **must** clone this repo and be in this environment in order to recreate our RFE Marmin (method 4) results.