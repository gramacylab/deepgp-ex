
"""
Implements DGP with DSVI (doubly stochastic variational inference)
using the "gpflux" package.

Uses the virtual environment specified in "dsvi_env.txt".

Whenever possible, we follow the defaults/examples provided at:
https://secondmind-labs.github.io/GPflux/
"""

import gpflow
import tensorflow as tf
from gpflux.architectures import Config
from gpflux.models import DeepGP
from scipy.cluster.vq import kmeans2
tf.keras.backend.set_floatx("float64")
tf.get_logger().setLevel("INFO")

from gpflow.likelihoods import Gaussian
from gpflux.helpers import (
    construct_basic_inducing_variables,
    construct_basic_kernel,
    construct_mean_function
)
from gpflux.layers.gp_layer import GPLayer
from gpflux.layers.likelihood_layer import LikelihoodLayer
from gpflow.kernels import Matern52

def my_build_constant_input_dim_deep_gp(X, num_layers, config, theta, tau2):
    
    num_data, input_dim = X.shape
    X_running = X
    gp_layers = []
    
    # Use Kmeans to initialize inducing points
    centroids, _ = kmeans2(X, k = config.num_inducing, minit = "points")

    for i_layer in range(num_layers):
        is_last_layer = (i_layer == num_layers - 1)
        D_in = input_dim
        if is_last_layer:
            D_out = 1
        else: 
            D_out = input_dim
        
        # Specify inducing variables
        inducing_var = construct_basic_inducing_variables(
            num_inducing = config.num_inducing, 
            input_dim = D_in, 
            share_variables = True, 
            z_init = centroids
        )
        
        # Construct kernel
        kernel = construct_basic_kernel(
            kernels = Matern52(variance = tau2[i_layer], lengthscales = theta),
            output_dim = D_out,
            share_hyperparams = True, 
        )

        # Build up mean function (zero mean for outer layer)
        if is_last_layer:
            mean_function = gpflow.mean_functions.Zero()
            q_sqrt_scaling = 1.0
        else:
            mean_function = construct_mean_function(X_running, D_in, D_out)
            X_running = mean_function(X_running)
            if tf.is_tensor(X_running):
                X_running = X_running.numpy()
            q_sqrt_scaling = config.inner_layer_qsqrt_factor
        
        # Construct GP layer
        layer = GPLayer(
            kernel,
            inducing_var,
            num_data,
            mean_function = mean_function,
            name = f"gp_{i_layer}"
        )
        layer.q_sqrt.assign(layer.q_sqrt * q_sqrt_scaling)
        gp_layers.append(layer)

    # Return DGP object
    likelihood = Gaussian(config.likelihood_noise_variance)
    return DeepGP(gp_layers, LikelihoodLayer(likelihood))

def DSVI(X, Y, XX, num_layers = 2, num_inducing = 100, g = 1e-4, 
         theta = None, tau2 = None):
    
    """
    Inputs
    ------
    X: training data inputs
    Y: training data output
    XX: testing data inputs
    num_layers: number of layers of deep-gp
    num_inducing: number of inducing points used in approximations
    g: nugget/noise parameter
    theta: lengthscale parameters (array of length equal to dimension of X)
    tau2: scale parameter (array of length equal to num_layers)
    
    Outputs
    -------
    mu: predicted mean
    var: predicted point-wise variances
    """
        
    # Set default values of lengthscales and scale (taken from gpflux examples)
    if theta is None: 
        theta = [2.0] * X.shape[1]
    if tau2 is None: 
        tau2 = [1e-6] * (num_layers - 1) + [1.0]
    
    # Establish configuration object 
    config = Config(
        num_inducing = num_inducing, 
        inner_layer_qsqrt_factor = 1e-5, 
        likelihood_noise_variance = g, 
        whiten = True # non-whitened case not implemented
    )
    
    # Fit DGP model
    deep_gp: DeepGP = my_build_constant_input_dim_deep_gp(X, 
                                                          num_layers, 
                                                          config, 
                                                          theta,
                                                          tau2)
    training_model: tf.keras.Model = deep_gp.as_training_model()
    training_model.compile(optimizer = tf.optimizers.Adam(learning_rate = 0.01))
    training_model.fit({"inputs": X, "targets": Y}, epochs = int(1e3), verbose = 0)
    
    # Make predictions at testing locations
    pred = deep_gp.as_prediction_model()(XX)
    mu = pred.f_mean.numpy()
    var = pred.f_var.numpy()
        
    return mu, var
    
    
    
    
    
