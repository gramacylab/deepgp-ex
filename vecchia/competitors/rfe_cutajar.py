
"""
Implements DGP with RFE (random feature expansion) using the git repository:
    https://github.com/mauriziofilippone/deep_gp_random_features
    (following Cutajar et al., 2017)

You must clone this repo in your home directory.

Uses the virtual environment specified in "rfe_cutajar_env.txt".

Whenever possible, we follow the defaults/examples in the above repository.
The default value of the nugget parameter is g~0.13, which is large for our
applications.  We change this default to g = 1e-4 for comparison against
other surrogate methods (although we've found this does not affect predictive
performance).
"""

from tensorflow.contrib.learn.python.learn.datasets import base
from tensorflow.python.framework import dtypes
import tensorflow as tf

import sys
from dataset import DataSet
import utils
import likelihoods
from dgp_rff import DgpRff
import losses
import numpy as np

def RFE(X, Y, XX, YY, num_layers = 2, g = 1e-4, var_setting = "var_fixed"):
    
    """
    Inputs
    ------
    X: training data inputs
    Y: training data output
    XX: testing data inputs
    YY: testing data output
    num_layers: number of layers of deep-gp
    g: nugget/noise parameter
    
    Outputs
    -------
    mu: predicted mean (predictive variances are not implemented)
    """
    n, d = X.shape
    lr = 0.01
    
    # Establish dataset objects, likelihood, and optimizers
    train = DataSet(X, Y)
    test = DataSet(XX, YY)
    error_rate = losses.RootMeanSqError(d) # used to be 1???
    likelihood = likelihoods.Gaussian(log_var = np.log(g, dtype = "float32"))
    optimizer = utils.get_optimizer("adam", lr)

    # Create DGP object
    dgp = DgpRff(likelihood_fun = likelihood,        
                 num_examples = n,     
                 d_in = d,            
                 d_out = 1,            
                 n_layers = num_layers,                
                 n_rff = 100,                  
                 df = d,               
                 kernel_type = "RBF",           
                 kernel_arccosine_degree = 0,   
                 is_ard = True,                
                 local_reparam = True,          
                 feed_forward = True,           
                 q_Omega_fixed = 1000,          
                 theta_fixed = 4000,            
                 learn_Omega = var_setting)

    # Fit DGP model
    dgp.learn(data = train, 
              learning_rate = lr, 
              mc_train = 100, 
              batch_size = min(n, 200), 
              n_iterations = 10000, 
              optimizer = optimizer, 
              display_step = 1000, 
              test = test,
              mc_test = 100, 
              loss_function = error_rate,  
              duration = 60,  
              less_prints = False) 
    
    # Make predictions at testing locations
    mu = dgp.predict(data = test, mc_test = 100)
    return mu[0]
