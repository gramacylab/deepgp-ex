
"""
Implements DGP with RFE (random feature expansion) using the git repository:
    https://github.com/mauriziofilippone/deep_gp_random_features
    (following Marmin and Filippone, 2022)
    
You must clone this repo in your home directory.

Uses the virtual environment specified in "rfe_marmin_env.txt".
"""

from os.path import join
from torch import tensor, float32
from vcal.nets import RegressionNet
from vcal.layers import FourierFeaturesGaussianProcess as GP
from torch.utils.data import DataLoader, TensorDataset
from torch.utils.data.dataset import random_split
from vcal.vardl_utils.logger import TensorboardLogger
from vcal.learning import TrainerNoCalib
from vcal.vardl_utils.initializers import IBLMInitializer
from vcal.utilities import SingleSpaceBatchLoader

TENSORBOARD_DIR = "tensorbord"

def VI_RFE(X, Y, XX, YY, seed, num_layers = 2, nfeatures = 100, g = 1e-4):

    """
    Inputs
    ------
    X: training data inputs
    Y: training data output
    XX: testing data inputs
    YY: testing data output
    seed: used for temporary directory
    num_layers: number of layers of deep-gp
    nfeatures: number of random features
    g: nugget/noise parameter
    
    Outputs
    -------
    mu: predicted mean
    std: predicted standard deviation
    """
    
    # Training params
    lr = 0.01
    optimizer = 'Adam'
    batch_size = 30
    time_budget = 120
    iterations = 10000    
    nmc_train=10
    precision = float32 # or "double"

    # Visualisation params
    test_interval = 10000
    train_log_interval = 10000
    nmc_test = 100

    # Start running
    d = X.shape[1]
    dout = 1
    device = "cpu" # or cpu

    # Prepare data
    x = tensor(X).type(precision)
    xx = tensor(XX).type(precision)
    mm = tensor(Y).type(precision).mean().item()
    y = tensor(Y).type(precision)-mm
    yy = tensor(YY).type(precision)-mm # of course not used until the end!!
    train_data = TensorDataset(x,y)
    test_data  = TensorDataset(xx,yy)

    train_loader = DataLoader(train_data,
                                batch_size=batch_size,
                                shuffle=True,
                                num_workers=0,
                                pin_memory=True)

    test_loader = DataLoader(test_data,
                                batch_size=batch_size,# * torch.cuda.device_count(),
                                shuffle=True,
                                num_workers=0,
                                pin_memory=True)

    
    layers = [GP(d, d, fix_output_stddev=True, nfeatures=nfeatures,nmc_train=nmc_train,nmc_test=nmc_test) for _ in range(num_layers-1)]
    layers += [GP(d, dout, nfeatures=nfeatures)]

    model = RegressionNet(layers=layers)

    if False:
        # take a subset of training data for initialization
        init_batchsize = 10000
        npts = len(train_loader.dataset)
        init_batchsize = min(init_batchsize,npts)
        init_data,_= random_split(train_loader.dataset,[init_batchsize,npts-init_batchsize])
        # Prepare the data ("SingleSpace" because in the more general setting of calibration, there is also a "calibration space")
        dataloader_for_init = SingleSpaceBatchLoader(DataLoader(init_data,batch_size=init_batchsize),cat_inputs=True)
        # initialize
        model_initializer=IBLMInitializer(model,dataloader_for_init,noise_var =1*(y.var()).mean().item())
        model_initializer.initialize()


    model.likelihood.stddevs = g
    
    tb_logger = TensorboardLogger(path=join(TENSORBOARD_DIR,"n_"+str(x.shape[0]),"rep_"+str(seed)), model=model, directory=None)

    optimizer_config = {'lr': lr}
    trainer = TrainerNoCalib( model,
                    optimizer, optimizer_config, train_loader, test_loader, device,
                 seed, tb_logger)
    print("tout")
    for p in model.parameters():
        print(p.shape)
    print("layer1")
    for p in model.layers[0].parameters():
        print(p.shape, end="; requ grad : ")
        print(p.requires_grad)
    print("layer2")
    for p in model.layers[1].parameters():
        print(p.shape, end="; requ grad : ")
        print(p.requires_grad)
    print("likel")
    for p in model.likelihood.parameters():
        print(p.shape, end="; requ grad : ")
        print(p.requires_grad)
    print("lay1 std requires grad")
    print(model.layers[-2]._stddevs.requires_grad)
    print("lay2 std requires grad")
    print(model.layers[-1]._stddevs.requires_grad)
    trainer.fit(iterations, test_interval, train_log_interval=train_log_interval, time_budget=time_budget)
    model.eval()
    
    samplePaths = mm + model.forward(xx)
    
    return samplePaths.mean(0).detach().numpy(), samplePaths.std(0).detach().numpy()
