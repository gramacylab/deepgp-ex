
"""
Evaluates python-implemented models on the 4D G function.

Requires specifed method/seed, but runs various training data sizes in a loop.
Methods include:
    DGP DSVI (1): doubly stochastic variational inference
    DGP HMC (2): hamiltonian monte carlo
    DGP RFE Cutajar (3): random feature expansion following Cutajar et al., 2017
    
Must be in the correct virtual environment:
    dsvi_env.txt for DSVI (method 1)
    hmc_env.txt for HMC (method 2)
    rfe_cutajar_env.txt for RFE (method 3)
    
See README.md in the "competitors" folder for details.
"""

import sys
import numpy as np
import pandas as pd
import properscoring as ps
import time

seed = int(sys.argv[1])
method = int(sys.argv[2])
save_time = (seed == 1)

if method == 1:
    sys.path.append("../competitors")
    import dsvi
    name = "dgpDSVI"
    num_inducing = 100
elif method == 2:
    sys.path.append("../../../sghmc_dgp")
    import models
    name = "dgpHMC"
elif method == 3:
    sys.path.append("../competitors")
    sys.path.append("../../../deep_gp_random_features/code")
    import rfe_cutajar
    name = "dgpRFE_Cutajar"

for n in [3000, 5000, 7000]:

    np.random.seed(seed)
    
    # Load pre-stored training and testing data
    train_name = "data/train_d4_n" + str(n) + "_seed" + str(seed) + ".csv"
    test_name  = "data/test_d4_seed" + str(seed) + ".csv"
    train = pd.read_csv(train_name)
    test  = pd.read_csv(test_name)
    X = np.array(train.iloc[:, :-1])
    Y = np.array(train["Y"]).reshape(-1, 1)
    XX = np.array(test.iloc[:, :-1])
    YY = np.array(test["Y"]).reshape(-1, 1)
    
    # Evaluate model and prediction metrics
    if method == 1:
        
        tic = time.time()
        mu, var = dsvi.DSVI(X, Y, XX, num_layers = 2, num_inducing = num_inducing)
        toc = time.time()

        rmse_value = np.sqrt(np.mean((mu - YY)**2))
        crps_value = np.mean(ps.crps_gaussian(YY, mu = mu, sig = np.sqrt(var)))
    
    elif method == 2:
        
        tic = time.time()
        model = models.RegressionModel()
        model.fit(X, Y)
        mu, var = model.predict(XX)
        toc = time.time()

        rmse_value = np.sqrt(np.mean((mu - YY)**2))
        crps_value = np.mean(ps.crps_gaussian(YY, mu = mu, sig = np.sqrt(var)))
    
    else:
        
        tic = time.time()
        mu = rfe_cutajar.RFE(X, Y, XX, YY, num_layers = 2)
        toc = time.time()

        rmse_value = np.sqrt(np.mean((mu - YY)**2))
        crps_value = None

    if save_time:
        t = pd.read_csv("time.csv")
        t.loc[len(t.index)] = [name, n, (toc - tic) / 60]
        t.to_csv("time.csv", index = False)

    results = pd.read_csv("results.csv")
    results.loc[len(results.index)] = [name, n, seed, rmse_value, crps_value]
    results.to_csv("results.csv", index = False)

