#!/bin/bash

# Activate virtual environment from terminal

method=2
reps=10

for (( seed=1; seed<=$reps; seed++ ))
do
    python g.py $seed $method &
done
