
# Four-dimensional G Function with Noise

More information about this function is available on the Virtual Library of Simulation Experiments: https://www.sfu.ca/~ssurjano/gfunc.html

* **data**: contains "csv" files of pre-stored training and testing data, as well as the `R` script used to generate them
* **g.py**: fits python-implemented models
* **g.R**: fits R-implemented models
* **results.csv**: contains results (RMSE and CRPS) for all models
* **run_python.sh**: bash script to run `g.py`
* **run_R.sh**: bash script to run `g.R`
* **time.csv**: contains computation time for a single seed