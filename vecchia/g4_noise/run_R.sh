#!/bin/bash

method=2
n=6000
reps=5

for (( seed=1; seed<=$reps; seed++ ))
do
  R CMD BATCH "--args seed=$seed method=$method n=$n" g.R $seed.Rout &
done
