#!/bin/bash

method=1
n=10000
reps=10

for (( seed=1; seed<=$reps; seed++ ))
do
  R CMD BATCH "--args seed=$seed method=$method n=$n" g.R $seed.Rout &
done
