
# Conditioning Set Size (m) Comparison

* **m.R**: fits GP SVEC and DGP VEC models with varying m values
* **results.csv**: contains results (RMSE and CRPS) for all models
* **run_R.sh**: bash script to run `m.R`
* **time.csv**: contains computation time for a single seed