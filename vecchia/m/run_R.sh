#!/bin/bash

method=1
reps=20
m=25

for (( seed=1; seed<=$reps; seed++ ))
do
  R CMD BATCH "--args seed=$seed method=$method m_train=$m m_test=$m" m.R $seed.Rout &
done
