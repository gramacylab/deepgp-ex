
# Seven-dimensional Satellite Drag Simulation

The work in this folder relies on pre-stored runs of the TPM simulator (Sun et al., 2019).  This data is available in
a public git repository: https://bitbucket.org/gramacylab/tpm/src/master/

We specifically used the `tpm/data/GRACE/graceH.dat` data file.

* **collect.R**: reads stored predictions (only for DGP VEC models), evaluates RMSE and CRPS, and writes results to "csv"
* **data**: contains "csv" files of pre-stored training and testing data *row indices*, as well as the `R` script used to generate them
* **initialization**: contains script used to learn separable lengthscales (using GP SVEC) for pre-scaling inputs and scripts to run a small DGP MCMC in order to seed/initialize larger chains at these burned-in values
* **predictions**: contains "csv" files of predictions from the DGP VEC models
* **results.csv**: contains results (RMSE and CRPS) for all models
* **run_python.sh**: bash script to run `satellite.py`
* **run_R.sh**: bash script to run `satellite.R`
* **satellite.py**: fits python-implemented models
* **satellite.R**: fits R-implemented models
* **setup.R**: contains frequently used functions including scaling of inputs/outputs
* **time.csv**: contains computation time for a single seed