#!/bin/bash

# Activate virtual environment from terminal

reps=10
method=1
n=100

for (( seed=1; seed<=$reps; seed++ ))
do
  python satellite.py $seed $method $n &
done
