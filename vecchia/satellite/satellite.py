
"""
Evaluates python-implemented models on the 7D satellite drag simulation.
Requires specifed method/seed/n.
Methods include:
    DGP DSVI (1): doubly stochastic variational inference
    DGP HMC (2): hamiltonian monte carlo
    
Must be in the correct virtual environment:
    dsvi_environment.txt for DSVI (method 1)
    hmc_environment.txt for HMC (method 2)
    
See README.md in the "competitors" folder for details.
"""

import sys
import numpy as np
import pandas as pd
import properscoring as ps
import time

seed = int(sys.argv[1])
method = int(sys.argv[2])
n = int(sys.argv[3])
save_time = (seed == 1)

if method == 1:
    sys.path.append("../competitors")
    import dsvi
    name = "dgpDSVI"
    num_inducing = 100
elif method == 2:
    sys.path.append("../../../sghmc_dgp")
    import models
    name = "dgpHMC"

# Set-up scaling
bounds = pd.DataFrame([[5500, 100, 200, 0, 0, -3.15, -1.58], 
                       [9500, 500, 2000, 1, 1, 3.14, 1.58]], 
                      columns = ["Umag", "Ts", "Ta", "alphan", "sigmat", "theta", "phi"])

def scale_down(X):
    for i in range(0, X.shape[1], 1):
        X[:,i] = (X[:,i] - bounds.iloc[0, i]) / (bounds.iloc[1, i] - bounds.iloc[0, i])
    return(X)

theta = [28.36, 47.02, 23.44, 0.81, 2.44, 0.03, 0.10]
def pre_scale(X):
    for i in range(0, X.shape[1], 1):
        X[:,i] = X[:,i] / theta[i]
    return(X)

Cd_mean = 2.9257
Cd_sd = 1.1942

# Import and scale data (including pre-scaling)
dat = np.array(pd.read_csv("~/tpm-git/data/GRACE/graceH.dat", sep = " "))
dat[:,0:6] = pre_scale(scale_down(dat[:,0:6]))
dat[:,7] = (dat[:,7] - Cd_mean) / Cd_sd

np.random.seed(seed)
    
# Load pre-stored training and testing data row indices
train_name = "data/train_index_n" + str(n) + "k_seed" + str(seed) + ".csv"
test_name  = "data/test_index_seed"  + str(seed) + ".csv"
train_inds = np.array(pd.read_csv(train_name)["x"])
test_inds  = np.array(pd.read_csv(test_name)["x"])
X = dat[(train_inds - 1), 0:6]
Y = dat[(train_inds - 1), 7].reshape(-1, 1)
XX = dat[(test_inds - 1), 0:6]
YY = dat[(test_inds - 1), 7].reshape(-1, 1)
YY_orig = YY * Cd_sd + Cd_mean

# Evaluate model and prediction metrics
if method == 1:

    tic = time.time()
    mu, var = dsvi.DSVI(X, Y, XX, num_layers = 2, num_inducing = num_inducing)
    toc = time.time()
    
    mean_orig = (mu * Cd_sd) + Cd_mean
    s2_orig = var * (Cd_sd**2)
    rmspe_value = np.sqrt(np.mean((100 * (mean_orig - YY_orig) / YY_orig)**2))
    crps_value = np.mean(ps.crps_gaussian(YY_orig, mu = mean_orig, sig = np.sqrt(s2_orig)))

elif method == 2:
    
    tic = time.time()
    model = models.RegressionModel()
    model.fit(X, Y)
    mu, var = model.predict(XX)
    toc = time.time()
    
    mean_orig = (mu * Cd_sd) + Cd_mean
    s2_orig = var * (Cd_sd**2)
    rmspe_value = np.sqrt(np.mean((100 * (mean_orig - YY_orig) / YY_orig)**2))
    crps_value = np.mean(ps.crps_gaussian(YY_orig, mu = mean_orig, sig = np.sqrt(s2_orig)))

if save_time:
    t = pd.read_csv("time.csv")
    t.loc[len(t.index)] = [name, n, (toc - tic) / 60]
    t.to_csv("time.csv", index = False)

results = pd.read_csv("results.csv")
results.loc[len(results.index)] = [name,  n, seed, rmspe_value, crps_value]
results.to_csv("results.csv", index = False)
    
    
