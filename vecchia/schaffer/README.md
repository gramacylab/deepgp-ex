
# Two-dimensional Schaffer Function

More information about this function is available on the Virtual Library of Simulation Experiments: https://www.sfu.ca/~ssurjano/schaffer4.html

* **data**: contains "csv" files of pre-stored training and testing data, as well as the `R` script used to generate them
* **results.csv**: contains results (RMSE and CRPS) from all models
* **run_python.sh**: bash script to run `schaffer.py`
* **run_R.sh**: bash script to run `schaffer.R`
* **schaffer.py**: fits python-implemented models
* **schaffer.R**: fits R-implemented models
* **time.csv**: contains computation time for a single seed
* **visualize.R**: produces a heat map and 3D surface plot of the Schaffer function