#!/bin/bash

method=2
n=500
reps=10

for (( seed=1; seed<=$reps; seed++ ))
do
  R CMD BATCH "--args seed=$seed method=$method n=$n" schaffer.R $seed.Rout &
done
