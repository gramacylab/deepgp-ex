#!/bin/bash

# Activate virtual environment from terminal

method=3
reps=10

for (( seed=1; seed<=$reps; seed++ ))
do
    python schaffer.py $seed $method &
done
