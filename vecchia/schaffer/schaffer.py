
"""
Evaluates python-implemented models on the 2D Schaffer function.

Requires specifed method/seed, but runs various training data sizes in a loop.
Methods include:
    DGP DSVI (1): doubly stochastic variational inference
    DGP HMC (2): hamiltonian monte carlo
    DGP RFE Cutajar (3): random feature expansion following Cutajar et al., 2017
    DGP RFE Marmin (4): random feature expansion following Marmin & Filippone, 2022
    
Must be in the correct virtual environment:
    dsvi_env.txt for DSVI (method 1)
    hmc_env.txt for HMC (method 2)
    rfe_cutajar_env.txt for RFE Cutajar (method 3)
    rfe_marmin_env.txt for RFE Marmin (method 4)
    
See README.md in the "competitors" folder for details.
"""

import sys
import numpy as np
import pandas as pd
import properscoring as ps
import time

seed = int(sys.argv[1])
method = int(sys.argv[2])
save_time = (seed == 1)

if method == 1:
    sys.path.append("../competitors")
    import dsvi
    name = "dgpDSVI"
elif method == 2:
    sys.path.append("../../../sghmc_dgp")
    import models
    name = "dgpHMC"
elif method == 3:
    sys.path.append("../competitors")
    sys.path.append("../../../deep_gp_random_features/code")
    import rfe_cutajar
    name = "dgpRFE_Cutajar"
else:
    sys.path.append("../competitors")
    sys.path.append("../../../variational-calibration")
    import rfe_marmin
    name = "dgpRFE_Marmin"

for n in [100, 500, 1000]:
    
    np.random.seed(seed)
        
    # Load pre-stored training and testing data
    train_name = "data/train_d2_n" + str(n) + "_seed" + str(seed) + ".csv"
    test_name  = "data/test_d2_seed" + str(seed) + ".csv"
    train = pd.read_csv(train_name)
    test  = pd.read_csv(test_name)
    X = np.array(train.iloc[:, :-1])
    Y = np.array(train["Y"]).reshape(-1, 1)
    XX = np.array(test.iloc[:, :-1])
    YY = np.array(test["Y"]).reshape(-1, 1)
    
    # Evaluate model and prediction metrics
    if method == 1:
        
        tic = time.time()
        num_inducing = np.min([n, 100])
        mu, var = dsvi.DSVI(X, Y, XX, num_layers = 2, num_inducing = num_inducing)
        toc = time.time()
        
        rmse_value = np.sqrt(np.mean((mu - YY)**2))
        crps_value = np.mean(ps.crps_gaussian(YY, mu = mu, sig = np.sqrt(var)))
        
    elif method == 2:
        
        tic = time.time()
        model = models.RegressionModel()
        model.fit(X, Y)
        mu, var = model.predict(XX)
        toc = time.time()
        
        rmse_value = np.sqrt(np.mean((mu - YY)**2))
        crps_value = np.mean(ps.crps_gaussian(YY, mu = mu, sig = np.sqrt(var)))
        
    elif method == 3: 
        
        tic = time.time()
        mu = rfe_cutajar.RFE(X, Y, XX, YY, num_layers = 2)
        toc = time.time()
        
        rmse_value = np.sqrt(np.mean((mu - YY)**2))
        crps_value = None

    elif method == 4:

        tic = time.time()
        mu, std = rfe_marmin.VI_RFE(X, Y, XX, YY, seed, num_layers = 2, nfeatures = 100)
        toc = time.time()
        
        rmse_value = np.sqrt(np.mean((mu - YY)**2))
        crps_value = np.mean(ps.crps_gaussian(YY, mu = mu, sig = std))

    if save_time:
        t = pd.read_csv("time.csv")
        t.loc[len(t.index)] = [name, n, (toc - tic) / 60]
        t.to_csv("time.csv", index = False)
        
    results = pd.read_csv("results.csv")
    results.loc[len(results.index)] = [name,  n, seed, rmse_value, crps_value]
    results.to_csv("results.csv", index = False)

