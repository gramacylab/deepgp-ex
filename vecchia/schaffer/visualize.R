
# Generates heat map and 3D surface plot of the Schaffer function

library(interp)
library(lhs)

f <- function(x) {
  z <- 4 * x - 2
  return(0.5 + (cos(sin(abs(z[,1]^2-z[,2]^2)))^2 - 0.5) / 
           (1 + 0.001*(z[,1]^2 + z[,2]^2))^2)
}

x <- lhs::randomLHS(5e4, 2)
y <- f(x)
i <- interp::interp(x[, 1], x[, 2], y)

par(mfrow = c(1, 2))
image(i, col = heat.colors(128))
contour(i, add = TRUE)
par(mar = c(1, 1, 1, 1))
persp(i, theta = 30, phi = 30, r = 30)

