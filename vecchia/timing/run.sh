#!/bin/bash

n=100
large=0 # zero for n <= 1000, 1 for larger vecchia-only timings
method=1

R CMD BATCH "--args method=$method n=$n large=$large" time.R &
